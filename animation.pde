//Fichier contenant ce qui relève des animations

//Méthodes qui à partir de l'index d'un sprite et de la largeur du cadre déduisent ses coordonnées sur la feuille
int yFromIndex(int index, int wCount)
{
  return (int)(index/wCount);
}

int xFromIndex(int index, int wCount)
{
  return index - yFromIndex(index, wCount)*wCount;
}

class BoundedAnimation  //Classe qui anime d'un sprite défini à un autre
{
  int start, end;           //Index du début et de la fin de l'animation
  int cycles;              //Le nombre de tous de l'animation que l'on a fait
  int w, h;               //Largeur et hauteur d'un sprite de la feuille
  int wCount, hCount;    //Nombre de ligne et colonnes de sprite
  int index, xSp, ySp;  //Numéro du sprite actuel et ses coordonnées en unité de sprite
  int sheet;           //L'index de feuille de sprite

  BoundedAnimation(int sheet, int w, int h, int start, int end)  //Constructeur
  {
    this.index = 0;  //On initialise les attributs
    this.xSp = 0;
    this.ySp = 0;
    //La fonction max(...) est appelée pour empêcher une division par 0
    this.w = max(1, w);
    this.h = max(1, h);
    this.sheet = sheet;
    this.wCount = max((int)(images.get(sheet).width/this.w), 1);
    this.hCount = max((int)(images.get(sheet).height/this.h), 1);

    if (end < start)         //On corrige d'éventuelles erreurs sur l'ordre du début et de la fin
    {
      end = start;
    }
    if (end > wCount*hCount-1)
    {
      end = wCount*hCount-1;
    }

    this.start = start;
    this.end = end;
    this.cycles = 0;
    this.index = start;  //On se place au début de l'animation
    setIndex(start);
  }

  BoundedAnimation(BoundedAnimation source)  //Constructeur de copie
  {
    this.index = source.index;  //On initialise les attributs
    this.xSp = source.xSp;
    this.ySp = source.ySp;
    this.w = source.w;
    this.h = source.h;
    this.sheet = source.sheet;
    this.wCount = source.wCount;
    this.hCount = source.hCount;
    this.start = source.start;
    this.end = source.end;
    this.cycles = source.cycles;
  }

  void animStep()  //Méthode passant au sprite suivant
  {
    xSp++;                 //On bouge le cadre d'un sprite à droite
    if (xSp+1 > wCount)    //S'il dépasse on le remet en début de ligne et on passe à la ligne suivante
    {
      xSp = 0;
      ySp++;
    }

    index = ySp*wCount+xSp;   //On met à jour l'index
    if (index > end)    //Si on dépasse la borne de fin, on revient à celle de début
    {
      setIndex(start);
      cycles++;
    }
  }

  int getCycles() //Méthode donnant le nombre de cycles de l'animation effectués
  {
    return cycles;
  }

  void setIndex(int i)  //Méthode pour choisir l'index
  {
    index = i;
    ySp = yFromIndex(index, wCount);    //On déduit les coordonnées à partir de l'index
    xSp = xFromIndex(index, wCount);
  }

  int getIndex()    //Méthode pour connaitre l'index
  {
    return index;
  }

  PImage getIcon() //Méthode retournant l'image actuelle de l'animation
  {
    return images.get(sheet).get(xSp*w, ySp*h, w, h);
  }

  void disp(float x, float y)    //Méthode d'affichage 
  {

    image(getIcon(), x, -y);    //On récupère la partie de l'image correspondant au sprite actuel et on l'affiche à l'endroit désiré
  }

  void reset() //Méthode réinitialisant l'animation
  {
    setIndex(start);
    cycles = 0;
  }
}

class SimpleAnimation extends BoundedAnimation  //Classe basique d'animation dont pourront hériter d'autres classes plus complexes
{

  SimpleAnimation(int sheet, int w, int h)  //Constructeur
  {
    super(sheet, w, h, 0, 0);
    this.end = wCount*hCount - 1; //La seule différence ici est que l'on n'a pas à préciser le début et la fin qui sont calculés pour être le début et la fin de la feuile
  }

  SimpleAnimation(BoundedAnimation bounded) //Constructeur pour convertir depuis une animation bornée
  {
    super(bounded.sheet, bounded.w, bounded.h, 0, 0);
    this.end = wCount*hCount - 1;
  }
}

String cleanPath(String path) //Fonction s'occupant de nettoyer un chemin de fichier
{
  /*
    Cette fonction regarde si elle trouve un accès à un sous dossier suivi d'un accès à son dossier
   parent et annule ces deux parties du chemin car inutiles, elle permet que plusieurs chemins relatifs
   pointant vers le même fichier soient transformés en le mếme chemin final.
   */
  boolean cleanedUp = false;
  while (!cleanedUp)
  {
    cleanedUp = true;
    String[] pathSplit = splitTokens(path, "/\\");
    String[] pathSplitCopy = splitTokens(path, "/\\");
    for (int i=1; i < pathSplit.length; i++)
    {
      if (pathSplitCopy[i].equals("..") && !pathSplitCopy[i-1].equals(".."))
      {
        pathSplit[i] = "";
        pathSplit[i-1] = "";
        cleanedUp = false;
      }
    }
    path = pathSplit[0];
    for (int i=1; i < pathSplit.length; i++)
    {
      if (!pathSplit[i].isEmpty())
      {
        path = path +"/"+pathSplit[i];
      }
    }
  }
  return path;
}

String folder(String path)  //Fonction récupérant le chemin du dossier parent d'un fichier
{
  path = cleanPath(path);
  String result = "";
  String[] pathSplit = splitTokens(path, "/\\");
  for (int i=0; i < pathSplit.length - 1; i++)
  {

    result = result + pathSplit[i]+"/";
  }
  return result;
}

class AnimationHandler  //Classe gérant plusieurs aimations et permettant de passer de l'une à l'autre
{
  int defaultAnimation; //Index de l'animation jouée par défaut
  int currentAnimation; //Index de l'animation en train d'être jouée
  int limitCycles;      //Nombre de cycles qu'il faut jouer l'animation
  BoundedAnimation[] animations;  //Tableau des animations

  AnimationHandler(int defaultAnimation, BoundedAnimation[] animations) //Constructeur
  {
    this.defaultAnimation = defaultAnimation; //Initialisation des attributs
    this.animations = animations;
    this.currentAnimation = defaultAnimation;
    this.limitCycles = -1;
  }

  AnimationHandler(AnimationHandler source) //Constructeur de copie
  {
    this.defaultAnimation = source.defaultAnimation;
    this.animations = new BoundedAnimation[source.animations.length];
    for (int i = 0; i < this.animations.length; i++)
    {
      this.animations[i] = new BoundedAnimation(source.animations[i]);
    }
    this.currentAnimation = source.currentAnimation;
    this.limitCycles = source.limitCycles;
  }

  void disp(float x, float y) //Méthode d'affichage
  {
    animations[currentAnimation].disp(x, y);
  }

  void step() //Méthode de cycle d'animation
  {
    animations[currentAnimation].animStep(); //On effectue une étape d'animation
    if (limitCycles >= 0 && animations[currentAnimation].getCycles() >= limitCycles) //Si l'animation n'est pas infiniement jouée et qu'on a dépassé le nombre de cycles
    {
      animations[currentAnimation].reset(); //On reset l'animation actuelle
      currentAnimation = defaultAnimation; //On repasse sur l'animation par défaut
      limitCycles = -1;
    }
  }

  //Assesseurs et mutateurs

  int getCurrentAnimation()
  {
    return currentAnimation;
  }

  void setDefaultAnimation(int index)
  {
    defaultAnimation = index;
  }

  int getDefaultAnimation()
  {
    return defaultAnimation;
  }

  PImage getIcon()
  {
    return animations[currentAnimation].getIcon();
  }

  void playAnimation(int index, int cycles) //Méthode pour jouer une animation précise
  {
    if (index >= 0 && index < animations.length && (animations[currentAnimation].getCycles()>limitCycles || index != currentAnimation)) //On vérifie qu'on ne sort pas du tableau
    {
      animations[currentAnimation].reset(); //On reset l'animation actuelle
      currentAnimation = index; //On se place sur l'animation demandée
      limitCycles = cycles; //On met à jour le nombre de cycles qu'on effectue
    }
  }

  void reset() //Méthode réinitialisant les animations
  {
    for (int i = 0; i < this.animations.length; i++)
    {
      this.animations[i].reset();
    }
  }
}