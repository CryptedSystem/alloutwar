//Ce fichier contient les fonctions qui relèvent de la lecture et de l'écriture de fichiers

//Fonctions de chargement pour les IAs

UnitAi[] loadUnitAis(String file, Unit body) //Fonction de chargement d'une ia depuis un fichier
{
  String[] lines = loadStrings(file);
  ArrayList<UnitAi> ais = new ArrayList<UnitAi>();
  for (int i = 0; i < lines.length; i++)
  {
    String formatted[] = lines[i].split("=");
    if (formatted.length == 1 && !formatted[0].trim().isEmpty())
    {
      ais.add(new UnitAi(folder(file)+formatted[0].trim(), body));
    }
  }
  return ais.toArray(new UnitAi[ais.size()]);
}

//Fonctions de chargement pour les animations

int loadSheet(String file)  //Fonction pour charger un fichier image
{

  if (!loadedImages.contains(file)) //Si l'image n'a pas déjà été chargée
  {
    if (new File(dataPath(file)).isFile()) //Si elle existe
    {
      loadedImages.add(file);      //On la charge
      images.add(loadImage(file));
      return images.size() - 1;     //On retourne sa position dans le tableau des images
    }
    else //Sinon on charge une image par défaut et on affiche une erreur
    {
      print("Error the file : "+file+" was not found, loading an error sprite instead.\n");
      return loadSheet("Error/ErrorSprite.png");
    }
  }
  else //Sinon
  {
    return loadedImages.indexOf(file); //On la trouve et on retourne sa position dans le tableau des images
  }
}

BoundedAnimation loadBoundedAnimation(String[] lines, String path) //Fonction pour charger une animation bornée depuis un fichier, non commentée car fonctionnant comme la précédente
{

  int h = 1;
  int w = 1;
  int start = 0;
  int end = 0;
  int sheet = 0;
  String sheetPath = "Error/ErrorSprite.png";
  for (int i = 0; i<lines.length; i++)
  {
    String[] formatted = lines[i].split("=");
    if (formatted.length >= 2)
    {
      formatted[0] = formatted[0].trim();
      if (formatted[0].equals("height"))
        h = Integer.parseInt(formatted[1].trim());
      else if (formatted[0].equals("width"))
        w = Integer.parseInt(formatted[1].trim());
      else if (formatted[0].equals("start"))
        start = Integer.parseInt(formatted[1].trim());
      else if (formatted[0].equals("end"))
        end = Integer.parseInt(formatted[1].trim());
      else if (formatted[0].equals("file"))
      {
        sheetPath = folder(path)+formatted[1].trim();
      }
    }
  }
  sheet = loadSheet(sheetPath);
  return new BoundedAnimation(sheet, w, h, start, end);
}

BoundedAnimation loadBoundedAnimation(String file) //Fonction pour charger une animation bornée depuis un fichier, non commentée car fonctionnant comme la précédente
{ 
  String[] lines = loadStrings("Error/Error.bld");
  if (new File(dataPath(file)).isFile())
  {
    lines = loadStrings(file);
  }
  else 
  {
    print("Error, the file : "+file+" was not found, loading an error file instead.\n");
  }
  return loadBoundedAnimation(lines, file);
}

SimpleAnimation loadSimpleAnimation(String[] lines, String path)  //Fonction pour charger une animation depuis un fichier
{
  return new SimpleAnimation(loadBoundedAnimation(lines, path));
}

SimpleAnimation loadSimpleAnimation(String file)  //Fonction pour charger une animation depuis un fichier
{
  return new SimpleAnimation(loadBoundedAnimation(file));
}

AnimationHandler loadAnimationHandler(String file) //Fonction pour charger un gestionnaire d'animations depuis un fichier 
{
  String[] lines = loadStrings("Error/Error.hdl");
  if (new File(dataPath(file)).isFile())
  {
    lines = loadStrings(file);
  }
  else 
  {
    print("Error, the file : "+file+" was not found, loading an error file instead.\n");
  }
  int defaultAnimation = 0;
  ArrayList<BoundedAnimation> animations = new ArrayList<BoundedAnimation>();
  int startLine = -1 ;
  int endLine = 0;
  for (int i = 0; i < lines.length; i++)
  {
    if (startLine < 0 && lines[i].contains("#"))
    {
      startLine = i + 1;
    }
    String[] formatted = split(lines[i], '=');
    if (formatted[0].trim().equals("default"))
    {
      defaultAnimation = Integer.parseInt(formatted[1].trim());
    }
  }
  boolean endReached = false; 
  while (!endReached && startLine < lines.length)
  {

    endLine = startLine;
    while (!endReached && !lines[endLine].contains("#"))
    {
      endLine++;
      if (endLine >= lines.length)
      {
        endReached = true;
      }
    }
    if (!endReached)
    {
      animations.add(loadBoundedAnimation(Arrays.copyOfRange(lines, startLine, endLine), file));
    }
    startLine = endLine + 1;
  }
  return new AnimationHandler(defaultAnimation, animations.toArray(new BoundedAnimation[animations.size()]));
}

//Fonctions de chargement pour les unités

Unit loadMeleeUnit(String file, float x, float y, String team) //Fonction pour charger une unité de mélée , fonctionne comme toutes les autres fonctions de chargement depuis un fihcier config
{
  String[] lines = loadStrings("Error/Error.unt");
  if (new File(dataPath(file)).isFile())
  {
    lines = loadStrings(file);
  }
  else 
  {
    print("Error, the file : "+file+" was not found, loading an error file instead.\n");
  }
  float speed = 0;
  float range = 0;
  int hp = 0;
  int damage = 0;
  String race = "Insect";  //Cuz it's a bug
  String name = "Coackroach";  //Same joke
  String handlerPath = "Error/Error.hdl";
  String mindPath = "Dumb.mnd";
  int cooldown = 0;
  float sizeX = 0;
  float sizeY = 0;
  int offX = 0;
  int offY = 0;
  int ironCost = 0, manaCost = 0;
  for (int i = 0; i<lines.length; i++)
  {
    String[] formatted = lines[i].split("=");
    if (formatted.length >= 2)
    {
      formatted[0] = formatted[0].trim();
      if (formatted[0].equals("name"))
        name = formatted[1].trim();
      else if (formatted[0].equals("race"))
        race = formatted[1].trim();
      else if (formatted[0].equals("animations"))
        handlerPath = folder(file)+formatted[1].trim();
      else if (formatted[0].equals("health"))
        hp = Integer.parseInt(formatted[1].trim());
      else if (formatted[0].equals("cooldown"))
        cooldown = Integer.parseInt(formatted[1].trim());
      else if (formatted[0].equals("range"))
        range = Float.parseFloat(formatted[1].trim());
      else if (formatted[0].equals("damage"))
        damage = Integer.parseInt(formatted[1].trim());
      else if (formatted[0].equals("speed"))
        speed = Float.parseFloat(formatted[1].trim());
      else if (formatted[0].equals("mind"))
        mindPath = folder(file)+formatted[1].trim();
      else if (formatted[0].equals("width"))
        sizeX = Float.parseFloat(formatted[1].trim());
      else if (formatted[0].equals("height"))
        sizeY = Float.parseFloat(formatted[1].trim());
      else if (formatted[0].equals("xOffset"))
        offX = Integer.parseInt(formatted[1].trim());
      else if (formatted[0].equals("yOffset"))
        offY = Integer.parseInt(formatted[1].trim());
      else if (formatted[0].equals("ironCost"))
        ironCost = Integer.parseInt(formatted[1].trim());
      else if (formatted[0].equals("manaCost"))
        manaCost = Integer.parseInt(formatted[1].trim());
    }
  }
  Unit unit = new Unit(x, y, sizeX, sizeY, speed, hp, range, cooldown, damage, name, race, team, loadAnimationHandler(handlerPath), offX, offY, ironCost, manaCost);
  unit.setAis(loadUnitAis(mindPath, unit));
  return unit;
}

RangeUnit loadRangeUnit(String file, float x, float y, String team) //Fonction pour charger une unité à distance
{

  String[] lines = loadStrings("Error/Error.unt");
  if (new File(dataPath(file)).isFile())
  {
    lines = loadStrings(file);
  }
  else 
  {
    print("Error, the file : "+file+" was not found, loading an error file instead.\n");
  }
  float speed = 0;
  float range = 0;
  float minRange = 0;
  int hp = 0;
  int damage = 0;
  int cooldown = 0;
  String race = "Insect";  //Cuz it's a bug
  String name = "Coackroach";  //Same joke
  String handlerPath = "Error/Error.hdl";
  String pjPath = "Error/Error.prj";
  String mindPath = "Dumb.mnd";
  float sizeX = 0;
  float sizeY = 0;
  int offX = 0, offY = 0;
  int ironCost = 0, manaCost = 0;
  for (int i = 0; i<lines.length; i++)
  {
    String[] formatted = lines[i].split("=");
    if (formatted.length >= 2)
    {
      formatted[0] = formatted[0].trim();
      if (formatted[0].equals("name"))
        name = formatted[1].trim();
      else if (formatted[0].equals("race"))
        race = formatted[1].trim();
      else if (formatted[0].equals("animations"))
        handlerPath = folder(file)+formatted[1].trim();
      else if (formatted[0].equals("health"))
        hp = Integer.parseInt(formatted[1].trim());
      else if (formatted[0].equals("minRange"))
        minRange = Float.parseFloat(formatted[1].trim());
      else if (formatted[0].equals("range"))
        range = Float.parseFloat(formatted[1].trim());
      else if (formatted[0].equals("speed"))
        speed = Float.parseFloat(formatted[1].trim());
      else if (formatted[0].equals("cooldown"))
        cooldown = Integer.parseInt(formatted[1].trim());
      else if (formatted[0].equals("projectile"))
        pjPath = folder(file)+formatted[1].trim();
      else if (formatted[0].equals("mind"))
        mindPath = folder(file)+formatted[1].trim();
      else if (formatted[0].equals("width"))
        sizeX = Float.parseFloat(formatted[1].trim());
      else if (formatted[0].equals("height"))
        sizeY = Float.parseFloat(formatted[1].trim());
      else if (formatted[0].equals("xOffset"))
        offX = Integer.parseInt(formatted[1].trim());
      else if (formatted[0].equals("yOffset"))
        offY = Integer.parseInt(formatted[1].trim());
      else if (formatted[0].equals("ironCost"))
        ironCost = Integer.parseInt(formatted[1].trim());
      else if (formatted[0].equals("manaCost"))
        manaCost = Integer.parseInt(formatted[1].trim());
    }
  }

  RangeUnit unit = new RangeUnit(x, y, sizeX, sizeY, speed, hp, minRange, range, cooldown, name, race, team, loadProjectile(pjPath), loadAnimationHandler(handlerPath), offX, offY, ironCost, manaCost);
  unit.setAis(loadUnitAis(mindPath, unit));
  return unit;
}

Unit loadUnit(String file, float x, float y, String team) //Fonction pour charger une unité quelconque
{
  String[] lines = loadStrings("Error/Error.unt");
  if (new File(dataPath(file)).isFile())
  {
    lines = loadStrings(file);
  }
  else 
  {
    print("Error, the file : "+file+" was not found, loading an error file instead.\n");
  }
  for (int i = 0; i<lines.length; i++)
  {
    String[] formatted = lines[i].split("=");
    if (formatted.length >= 2)
    {
      formatted[0] = formatted[0].trim();
      if (formatted[0].equals("type"))
      {
        if (formatted[1].trim().equals("melee"))
          return loadMeleeUnit(file, x, y, team);
        else if (formatted[1].trim().equals("range"))
          return loadRangeUnit(file, x, y, team);
      }
    }
  }
  return null;
}

Projectile loadProjectile(String file) //Fonction pour charger un projectile
{
  String[] lines = loadStrings("Error/Error.prj");
  if (new File(dataPath(file)).isFile())
  {
    lines = loadStrings(file);
  }
  else 
  {
    print("Error, the file : "+file+" was not found, loading an error file instead.\n");
  }
  float speed = 0;
  int damage = 0;
  String handlerPath = "Error/Error.hdl";
  for (int i = 0; i<lines.length; i++)
  {
    String[] formatted = lines[i].split("=");
    if (formatted.length >= 2)
    {
      formatted[0] = formatted[0].trim();
      if (formatted[0].equals("animations"))
        handlerPath = folder(file)+formatted[1].trim();
      else if (formatted[0].equals("damage"))
        damage = Integer.parseInt(formatted[1].trim());
      else if (formatted[0].equals("speed"))
        speed = Float.parseFloat(formatted[1].trim());
    }
  }

  return new Projectile(speed, damage, loadAnimationHandler(handlerPath));
}

//Fonctions de chargement pour les bâtiments

TrainingBuilding loadTrainingBuilding(String file, float x, float y, String team) //Fonction pour charger un bâtiment d'entrainement
{
  String[] lines = loadStrings("Error/Error.bld");
  if (new File(dataPath(file)).isFile())
  {
    lines = loadStrings(file);
  }
  else 
  {
    print("Error, the file : "+file+" was not found, loading an error file instead.\n");
  }
  ArrayList<Unit> produceable = new ArrayList<Unit>();
  String animPath = "Error/Error.anim";
  String name = "Nest";
  float sizeX = 0, sizeY = 0;
  int hp = 0;
  int laneLength = 0;
  int produceTime = 0;
  int ironCost = 0, manaCost = 0;
  int listStart = 0;

  for (int i = 0; i<lines.length; i++)
  {
    String[] formatted = lines[i].split("=");
    if (lines[i].contains("#Unit List#"))
    {
      listStart = i;
    }
    if (formatted.length >= 2)
    {
      formatted[0] = formatted[0].trim();     
      if (formatted[0].equals("animation"))
        animPath = folder(file)+formatted[1].trim();
      else if (formatted[0].equals("health"))
        hp = Integer.parseInt(formatted[1].trim());
      else if (formatted[0].equals("width"))
        sizeX = Float.parseFloat(formatted[1].trim());
      else if (formatted[0].equals("height"))
        sizeY = Float.parseFloat(formatted[1].trim());
      else if (formatted[0].equals("lane"))
        laneLength = Integer.parseInt(formatted[1].trim());
      else if (formatted[0].equals("speed"))
        produceTime = Integer.parseInt(formatted[1].trim());
      else if (formatted[0].equals("name"))
        name = formatted[1].trim();
      else if (formatted[0].equals("ironCost"))
        ironCost = Integer.parseInt(formatted[1].trim());
      else if (formatted[0].equals("manaCost"))
        manaCost = Integer.parseInt(formatted[1].trim());
    }
  }
  int endLine = listStart+1;
  while (endLine < lines.length)
  {
    if (lines[endLine].contains("#"))
    {
      break;
    }
    produceable.add(loadUnit(folder(file)+lines[endLine].trim(), x, y, team));
    endLine++;
  }
  return new TrainingBuilding(x, y, sizeX, sizeY, hp, laneLength, produceTime, produceable.toArray(new Unit[produceable.size()]), loadBoundedAnimation(animPath), team, name, ironCost, manaCost);
}

DefenseBuilding loadDefenseBuilding(String file, float x, float y, String team) //Fonction pour charger un bâtiment de défense
{
  String[] lines = loadStrings("Error/Error.bld");
  if (new File(dataPath(file)).isFile())
  {
    lines = loadStrings(file);
  }
  else 
  {
    print("Error, the file : "+file+" was not found, loading an error file instead.\n");
  }
  String animPath = "Error/Error.anim";
  String name = "Nest";
  float sizeX = 0, sizeY = 0;
  int hp = 0;
  float minRange = 0, maxRange = 0;
  int cooldown = 0;
  String projectilePath = "Error/Error.prj";
  int ironCost = 0, manaCost = 0;

  for (int i = 0; i<lines.length; i++)
  {
    String[] formatted = lines[i].split("=");
    if (formatted.length >= 2)
    {
      formatted[0] = formatted[0].trim();
      if (formatted[0].equals("animation"))
        animPath = folder(file)+formatted[1].trim();
      else if (formatted[0].equals("health"))
        hp = Integer.parseInt(formatted[1].trim());
      else if (formatted[0].equals("width"))
        sizeX = Float.parseFloat(formatted[1].trim());
      else if (formatted[0].equals("height"))
        sizeY = Float.parseFloat(formatted[1].trim());
      else if (formatted[0].equals("cooldown"))
        cooldown = Integer.parseInt(formatted[1].trim());
      else if (formatted[0].equals("name"))
        name = formatted[1].trim();
      else if (formatted[0].equals("projectile"))
        projectilePath = folder(file)+formatted[1].trim();
      else if (formatted[0].equals("minRange"))
        minRange = Float.parseFloat(formatted[1].trim());
      else if (formatted[0].equals("maxRange"))
        maxRange = Float.parseFloat(formatted[1].trim());
      else if (formatted[0].equals("ironCost"))
        ironCost = Integer.parseInt(formatted[1].trim());
      else if (formatted[0].equals("manaCost"))
        manaCost = Integer.parseInt(formatted[1].trim());
    }
  }

  return new DefenseBuilding(x, y, sizeX, sizeY, minRange, maxRange, cooldown, hp, loadBoundedAnimation(animPath), team, name, loadProjectile(projectilePath), ironCost, manaCost);
}

ResourceBuilding loadResourceBuilding(String file, float x, float y, String team) //Fonction pour charger un bâtiment de ressources
{
  String[] lines = loadStrings("Error/Error.bld");
  if (new File(dataPath(file)).isFile())
  {
    lines = loadStrings(file);
  }
  else 
  {
    print("Error, the file : "+file+" was not found, loading an error file instead.\n");
  }
  String animPath = "Error/Error.anim";
  String name = "Nest";
  float sizeX = 0, sizeY = 0;
  int hp = 0;
  int amount = 0;
  float rate = 0;
  Resource resource = Resource.Iron;
  int ironCost = 0, manaCost = 0;

  for (int i = 0; i<lines.length; i++)
  {
    String[] formatted = lines[i].split("=");
    if (formatted.length >= 2)
    {
      formatted[0] = formatted[0].trim();
      if (formatted[0].equals("animation"))
        animPath = folder(file)+formatted[1].trim();
      else if (formatted[0].equals("health"))
        hp = Integer.parseInt(formatted[1].trim());
      else if (formatted[0].equals("width"))
        sizeX = Float.parseFloat(formatted[1].trim());
      else if (formatted[0].equals("height"))
        sizeY = Float.parseFloat(formatted[1].trim());
      else if (formatted[0].equals("amount"))
        amount = Integer.parseInt(formatted[1].trim());
      else if (formatted[0].equals("rate"))
        rate = Float.parseFloat(formatted[1].trim());
      else if (formatted[0].equals("name"))
        name = formatted[1].trim();
      else if (formatted[0].equals("resource"))
        resource = Resource.valueOf(formatted[1].trim());
      else if (formatted[0].equals("ironCost"))
        ironCost = Integer.parseInt(formatted[1].trim());
      else if (formatted[0].equals("manaCost"))
        manaCost = Integer.parseInt(formatted[1].trim());
    }
  }

  return new ResourceBuilding(x, y, sizeX, sizeY, hp, resource, amount, rate, loadBoundedAnimation(animPath), team, name, ironCost, manaCost);
}

Building loadSimpleBuilding(String file, float x, float y, String team) //Fonction pour charger un bâtiment de ressources
{
  String[] lines = loadStrings("Error/Error.bld");
  if (new File(dataPath(file)).isFile())
  {
    lines = loadStrings(file);
  }
  else 
  {
    print("Error, the file : "+file+" was not found, loading an error file instead.\n");
  }
  String animPath = "Error/Error.anim";
  String name = "Nest";
  float sizeX = 0, sizeY = 0;
  int hp = 0;
  int ironCost = 0, manaCost = 0;

  for (int i = 0; i<lines.length; i++)
  {
    String[] formatted = lines[i].split("=");
    if (formatted.length >= 2)
    {
      formatted[0] = formatted[0].trim();
      if (formatted[0].equals("animation"))
        animPath = folder(file)+formatted[1].trim();
      else if (formatted[0].equals("health"))
        hp = Integer.parseInt(formatted[1].trim());
      else if (formatted[0].equals("width"))
        sizeX = Float.parseFloat(formatted[1].trim());
      else if (formatted[0].equals("height"))
        sizeY = Float.parseFloat(formatted[1].trim());
      else if (formatted[0].equals("name"))
        name = formatted[1].trim();
      else if (formatted[0].equals("ironCost"))
        ironCost = Integer.parseInt(formatted[1].trim());
      else if (formatted[0].equals("manaCost"))
        manaCost = Integer.parseInt(formatted[1].trim());
    }
  }

  return new Building(x, y, sizeX, sizeY, hp, loadBoundedAnimation(animPath), team, name, ironCost, manaCost);
}

Building loadBuilding(String file, float x, float y, String team) //Fonction pour charger un bâtiment quelconque
{
  String[] lines = loadStrings("Error/Error.bld");
  if (new File(dataPath(file)).isFile())
  {
    lines = loadStrings(file);
  }
  else 
  {
    print("Error, the file : "+file+" was not found, loading an error file instead.\n");
  }
  for (int i = 0; i<lines.length; i++)
  {
    String[] formatted = lines[i].split("=");
    if (formatted.length >= 2)
    {
      formatted[0] = formatted[0].trim();
      if (formatted[0].equals("type"))
      {
        if (formatted[1].trim().equals("resource"))
          return loadResourceBuilding(file, x, y, team);
        else if (formatted[1].trim().equals("defense"))
          return loadDefenseBuilding(file, x, y, team);
        else if (formatted[1].trim().equals("training"))
          return loadTrainingBuilding(file, x, y, team);
        else if (formatted[1].trim().equals("decoration"))
          return loadSimpleBuilding(file, x, y, "Environment");
      }
    }
  }
  return null;
}

void loadMap(String file) //Procédure pour charger une carte
{
  String[] lines = loadStrings("Error/Error.map"); //On charge un fichier par défaut

  if (new File(dataPath(file)).isFile())       //Si la carte existe on la charge
  {
    lines = loadStrings(file);
  }
  else //Sinon le fichier par défaut sera utilisé et on affiche une erreur
  {
    print("Error, the file : "+file+" was not found, loading an error file instead.\n");
  }

  int bStartLine = -1; //Ligne de départ de la liste des bâtiments
  int bEndLine = -1;   //Ligne de fin de la liste des bâtiments
  int uStartLine = -1;  //Ligne de départ de la liste des unités
  int uEndLine =-1;     //Ligne de fin de la liste des unités
  ArrayList<Building> preLoadedB = new ArrayList<Building>();
  ArrayList<Unit> preLoadedU = new ArrayList<Unit>();
  for (int i = 0; i < lines.length; i++)  //On charge les bâtiments utilisés en mémoire
  {
    if(lines[i].contains("="))
    {
      String[] formatted = lines[i].split("=");
      if(formatted[0].trim().equals("width"))
        EAST = Float.parseFloat(formatted[1].trim());
      else if(formatted[0].trim().equals("height"))
        NORTH = Float.parseFloat(formatted[1].trim());
    }
  }
  for (int i = 0; i < lines.length; i++)  //On charge les bâtiments utilisés en mémoire
  {
    if (bStartLine == -1)
    {
      if (lines[i].contains("#Buildings#"))
      {
        bStartLine = i;
      }
    }
    else if (lines[i].contains("#"))
    {
      bEndLine = i;
      break;
    }
    else
    {
      preLoadedB.add(loadBuilding(folder(file)+lines[i].trim(), 0, 0, "Swarm"));
    }
  }

  for (int i = bEndLine + 1; i < lines.length && !lines[i].contains("#"); i++) //On crée les bâtimentd à placer
  {
    String[] formatted = lines[i].split(";");
    float x = Float.parseFloat(formatted[0].trim());
    float y = Float.parseFloat(formatted[1].trim());
    int id = Integer.parseInt(formatted[2].trim());
    String team = formatted[3].trim();
    Building newBuilding = preLoadedB.get(id).cloneBuilding();
    newBuilding.setX(x);
    newBuilding.setY(y);
    newBuilding.setTeam(team);
    addBuilding(newBuilding); //Performances à comparer avec le tri de la liste une fois remplie
  }

  //On fait de même pour les unités 

  for (int i = 0; i < lines.length; i++) 
  {
    if (uStartLine == -1)
    {
      if (lines[i].contains("#Units#"))
      {
        uStartLine = i;
      }
    }
    else if (lines[i].contains("#"))
    {
      uEndLine = i;
      break;
    }
    else
    {
      preLoadedU.add(loadUnit(folder(file)+lines[i].trim(), 0, 0, "Swarm"));
    }
  }

  for (int i = uEndLine + 1; i < lines.length && !lines[i].contains("#"); i++)
  {
    String[] formatted = lines[i].split(";");
    float x = Float.parseFloat(formatted[0].trim());
    float y = Float.parseFloat(formatted[1].trim());
    int id = Integer.parseInt(formatted[2].trim());
    String team = formatted[3].trim();
    Unit newUnit = preLoadedU.get(id).cloneUnit();
    newUnit.setX(x);
    newUnit.setY(y);
    newUnit.setTeam(team);
    if (!team.equals(playerTeam))
    {
      newUnit.setCurrentAi(AiMode.Autonomous);
    }
    units.add(newUnit);
  }

  //Collections.sort(buildings, PositionComparator);  //On trie les unités par position pour afficher celles plus basses par dessus celles plus hautes
  //Performances à comparer avec l'utilisation de addBuilding(...) plus tôt
}

void loadAvailableBuildings(String file) //Procédure chargeant les bâtiments disponibles au joueur depuis un fichier
{
  String[] lines = loadStrings("Error/Error.avl");
  if (new File(dataPath(file)).isFile())
  {
    lines = loadStrings(file);
  }
  else 
  {
    print("Error, the file : "+file+" was not found, loading an error file instead.\n");
  }
  for (int i = 0; i < lines.length; i++)
  {
    availableBuildings.add(loadBuilding(folder(file)+lines[i].trim(), 0, 0, playerTeam));
  }
}
