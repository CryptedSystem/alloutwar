//Fichier contenant ce qui relève des boutons

//Procédures additionnelles 
void funFunction()
{
  //Effet graphique pour le pointeur
  frameRate(80);
  smooth();
  stroke(255, 0, 0);
  line(pmouseX, pmouseY, mouseX, mouseY);
}

void canevas(int wC, int hC) 
{
  //Cadre pour les boutons de confirmation ou menu in-game
  smooth();
  fill(165, 7, 50);
  stroke(255, 0, 0);
  rect((width/2)-wC/2, (height/2)-hC/2, wC, hC);
}

//Procédures indispensables

void setInvisible(ArrayList<? extends GAbstractControl> list, GAbstractControl exception, Boolean choice) 
{  
  //Rendre visible ou non une liste de boutons
  for (int i = 0; i < list.size(); i++)
  {
    if (list.get(i) != exception)
    {
      list.get(i).setVisible(!choice);
    }
    else 
    {
      list.get(i).setVisible(choice);
    }
  }
}


void setBooleans(ArrayList<Boolean> list, Boolean choice)
{ 
  //Activer ou non une liste de booleans
  for (int i = 0; i < list.size(); i++)
  {
    list.set(i, choice);
  }
}

void uiPosition() //Procédure repositionnant l'interface utilisateur selon la taille de la fenêtre
{ 
  //Recalcul de la position de chaque bouton en fonction de la taille de la fenêtre
  if (pWidth != width || pHeight != height)
  {
    float nx, ny; //Utlisé pour recalculer la nouvelle position des boutons
    //Fenêtre redimensionnée donc :
    //Replacer le texte
    pTx = width/2; //Variables de position du texte
    pTy = height/2;
    //Déplacer Play & Resume au centre
    nx = (width/2)-(wBtn/2);
    ny = (height/2)-2*hBtn-30;
    btnPlay.moveTo(nx, ny);
    btnResume.moveTo(nx, ny);
    //Déplacer Help to right middle
    nx = (width/2)-(wBtn/2);
    ny = (height/2)-hBtn-10;
    btnHelp.moveTo(nx, ny);
    //Déplacer Credits & MainMenu au centre
    nx = (width/2)-(wBtn/2);
    ny = (height/2)+10;
    btnCredits.moveTo(nx, ny);
    btnMainMenu.moveTo(nx, ny);
    //Déplacer Quit au centre
    nx = (width/2)-(wBtn/2);
    ny = (height/2)+hBtn+30;
    btnQuit.moveTo(nx, ny);
    //Déplacer YES & NO
    ny = (height/2)-(hBtn/4);
    nx = (width/2)-wBtn/3-20; //YES
    btnYes.moveTo(nx, ny);
    nx = (width/2)+20; //NO
    btnNo.moveTo(nx, ny);
    //Déplacer les boutons d'action en bas
    ny = height-dimActionBtn-20;
    btnAttack.moveTo(20, ny);    
    btnMove.moveTo(dimActionBtn+28, ny);    
    btnAutonomous.moveTo(2*dimActionBtn+36, ny);
    btnStop.moveTo(3*dimActionBtn+44, ny);

    for (int i = 0; i < trainingButtons.size(); i++)
    {
      trainingButtons.get(i).moveTo(10 + i*(dimDynamicButton+10), ny);
    }

    for (int i = 0; i < buildingButtons.size(); i++)
    {
      buildingButtons.get(i).moveTo(10 + i*(dimDynamicButton+10), ny);
    }

    resourceX = width - 300;
    resourceY = 50;

    //Sauvegarder la taille actuelle de la fenêtre
    pWidth = width;
    pHeight = height;
  }
}

void displayConfirmationButtons()
{
  //Affichage ou non des boutons en fonction de booleans
  btnNo.setVisible(buttonsBoolean.get(4));
  btnYes.setVisible(buttonsBoolean.get(4));
  btnBack.setVisible(buttonsBoolean.get(1) || buttonsBoolean.get(2));
  btnInGameMenu.setVisible(buttonsBoolean.get(0));
}

void displayActionButtons() 
{
  setInvisible(actionButtons, null, !(selectedUnits.size() > 0) || inMenu);
}

void displayBuildingButtons()
{
  setInvisible(buildingButtons, null, training || !buttonsBoolean.get(0) || selectedUnits.size() > 0);
}

void displayTrainingButtons()
{
  setInvisible(trainingButtons, null, !(training && buttonsBoolean.get(0)));
}

//Procédure affichant le menu en jeu
void displayInGameMenu()
{
  setInvisible(inGameButtons, btnInGameMenu, false);

  setBooleans(buttonsBoolean, false);
  buttonsBoolean.set(5, true); //inGameMenu
  inMenu = true;

  btnQuit.setVisible(true);
  btnHelp.setVisible(true);
}

void displayMainMenu(boolean choice)
{
}

void confirmationScreen() 
{
  setInvisible(menuButtons, null, true);
  setInvisible(inGameButtons, null, true);

  setBooleans(buttonsBoolean, false);
  buttonsBoolean.set(4, true); //Quit
}

//Procédure principale gérant les boutons

void handleButtonEvents(GButton button, GEvent event)
{
  //#### BOUTONS D'ACTION ####\\
  //Bouton attaque
  if (button == btnAttack && event == GEvent.CLICKED)
  {
    toggleAttackMode();
  }
  //Bouton Déplacement
  if (button == btnMove && event == GEvent.CLICKED)
  {
    toggleMoveMode();
  }
  //Bouton d'activité autonome
  if (button == btnAutonomous && event == GEvent.CLICKED)
  {
    setSelectionAi(AiMode.Autonomous);
  }
  //Bouton d'interruption
  if (button == btnStop && event == GEvent.CLICKED)
  {
    setSelectionAi(AiMode.Guard);
  }
}

void handleButtonEvents(GImageButton button, GEvent event)
{
  //Fonction principale de gestion des actions avec les boutons

  //#### BOUTONS MENU ####\\

  //Clic sur "Play"             (OK)
  if (button == btnPlay && event == GEvent.CLICKED)
  {
    setInvisible(menuButtons, null, true);               //Cacher tous les boutons sans exceptions

    setBooleans(buttonsBoolean, false);               //Désactiver tous les booleans
    buttonsBoolean.set(0, true);                     //Activer Play
    inGame = true;
    inMenu = false;

    surface.setLocation(0, 0);                     //Changer taille et position de la fenêtre à l'écran
    surface.setSize(displayWidth, displayHeight);
        
    travelX = 0;
    travelY = height;
  }

  //Clic sur "Quit"            (OK)
  if (button == btnQuit && event == GEvent.CLICKED)
  {
    //Afficher message : Are you sure you want to quit AllOutWar ?
    confirmationScreen();
  }  

  //Clic sur "Back"            (OK)
  if (button == btnBack && event == GEvent.CLICKED)
  {
    if (!inGame)
    {                                             //Retour à l'écran principal
      setInvisible(menuButtons, null, false);

      setBooleans(buttonsBoolean, false);
      buttonsBoolean.set(3, true); //Menu
    }
    else
    {                                      //Retour au menu in-game
      displayInGameMenu();
    }
  }

  //#### BOUTONS IN-GAME ####\\

  //Clic sur "Menu"              (OK)
  if (button == btnInGameMenu && event == GEvent.CLICKED)
  {
    displayInGameMenu();
  }
  //Clic sur "Resume"            (OK)
  if (button == btnResume && event == GEvent.CLICKED)
  {
    setInvisible(inGameButtons, null, true);
    setInvisible(menuButtons, null, true);

    setBooleans(buttonsBoolean, false);
    buttonsBoolean.set(0, true); //Play
    inMenu = false;
  }
  //Clic sur "Quit to main Menu"  (OK)
  if (button == btnMainMenu && event == GEvent.CLICKED)
  {
    confirmationScreen();
    mainMenu = true;
  }

  //#### AUTRES BOUTONS ####\\

  //Clic sur "Yes"/"No" 
  if (button == btnYes && event == GEvent.CLICKED)
  { //YES (OK)
    if (mainMenu)
    {                                 //Confirmation de retour au menu principal
      setInvisible(menuButtons, null, false);
      setBooleans(buttonsBoolean, false);
      buttonsBoolean.set(3, true);
      inGame = false;
      mainMenu = false;      

      surface.setSize(600, 600);
      surface.setLocation((displayWidth/2)-300, (displayHeight)/2-300);

      resetGame();
    }
    else
    {                        //Confirmation de quitter le jeu
      exit();
    }
  }
  if (button == btnNo && event == GEvent.CLICKED)
  { //NO
    if (mainMenu)
    {                                //Annulation et retour au menu in-game
      mainMenu = false;
      displayInGameMenu();
    }
    else
    {
      if (inGame)
      {                               //Annulation et retour au menu in-game
        mainMenu = false;
        displayInGameMenu();
      }
      else 
      {                                    //Annulation et retour au menu principal
        setInvisible(menuButtons, btnBack, false);

        buttonsBoolean.set(3, true);   //Menu
        buttonsBoolean.set(4, false); //Quit
      }
    }
  }

  //#### AUTRES BOUTONS MENU ####\\

  //Clic sur "Help"     (OK)
  if (button == btnHelp && event == GEvent.CLICKED)
  {
    setInvisible(menuButtons, null, true);
    setInvisible(inGameButtons, null, true);

    setBooleans(buttonsBoolean, false);
    buttonsBoolean.set(1, true); //Help
  }

  //Clic sur "Credits"  (OK)
  if (button == btnCredits && event == GEvent.CLICKED)
  {
    setInvisible(menuButtons, btnBack, true);
    setBooleans(buttonsBoolean, false);
    buttonsBoolean.set(2, true); //Credits
  }
}

void destroyButtons(ArrayList<GButton> buttons) //Procédure détruisant les boutons dans une liste et vidant cette liste
{
  for (int i = 0; i < buttons.size(); i++)
  {
    buttons.get(i).dispose();
  }
  buttons.clear();
}

public class BuildButtonHandler //Classe permettant de créer des boutons de gérer un bouton de construction de bâtiment 
{
  Building toBuild; //Bâtiment à construire

  BuildButtonHandler(Building toBuild, GButton button) //Constructeur
  {
    this.toBuild = toBuild;  //Initialisation des attributs
    button.addEventHandler(this, "handleButton"); //Association de la gestion des évènements du bouton à cet objet
  }

  public void handleButton(GButton button, GEvent event)  //Méthode gérant les évènements du bouton
  {
    placingBuilding = true;
    toPlace = toBuild;
  }
}

void generateBuildingButtons()    //Procédure générant les boutons de construction à partir de la liste des bâtiments disponibles au joueur
{
  for (int i = 0; i < availableBuildings.size(); i++) //On parcourt la liste
  {
    int manaCost = availableBuildings.get(i).getManaCost(); //On récupère des informations pour le texte du bouton
    int ironCost = availableBuildings.get(i).getIronCost();

    //On crée le bouton
    GButton button = new GButton(this, 10+i*(dimDynamicButton+10), height-dimActionBtn-20, dimDynamicButton, dimDynamicButton/2, "["+availableBuildings.get(i).getName()+"]\nIron : "+str(ironCost)+"\nMana : "+str(manaCost));

    //On crée son gestionnaire d'évènement, pas besoin de stocker sa référence, il sera détruit quand le bouton le sera
    new BuildButtonHandler(availableBuildings.get(i), button);

    buildingButtons.add(button);  //On ajoute le bouton à la liste
  }
}

public class TrainingButtonHandler  //Gestionnaire des boutons d'entrainement
{

  /* Cette classe fonctionne comme le gestionnaire de boutons de construction mais garde une référence
   à un bâtiment d'entrainement ainsi que l'index de l'unité à former.
   */

  TrainingBuilding trainer;
  int index;

  TrainingButtonHandler(TrainingBuilding trainer, int index, GButton button)
  {
    this.index = index;
    this.trainer = trainer;
    button.addEventHandler(this, "handleButton");
  }

  public void handleButton(GButton button, GEvent event)
  {
    int manaCost = trainer.getUnitList()[index].getManaCost();
    int ironCost = trainer.getUnitList()[index].getIronCost();
    //Si le joueur a assez de ressources pour le former et que la formation se passe sans accros
    if (playerMana >= manaCost && playerIron >= ironCost && trainer.train(index))  
    {
      //On déduit les coûts de ses ressources
      playerMana -= manaCost;
      playerIron -= ironCost;
    }
  }
}

void generateTrainingButtons(Unit[] list, TrainingBuilding building) //Procédure générant les boutons de formation d'unités
{
  //Fonction comme la procédure generateBuildingButtons()

  destroyButtons(trainingButtons);
  for (int i = 0; i < list.length; i++)
  {
    int manaCost = list[i].getManaCost();
    int ironCost = list[i].getIronCost();
    GButton button = new GButton(this, 10+i*(dimDynamicButton+10), height-dimActionBtn-20, dimDynamicButton, dimDynamicButton/2, "["+list[i].getName()+"]\nIron : "+str(ironCost)+"\nMana : "+str(manaCost));
    new TrainingButtonHandler(building, i, button);
    trainingButtons.add(button);
  }
}

void displayResource() //Procédure affichant les ressources du joueur
{
  stroke(255);
  fill(255);
  text("Iron : "+str(playerIron), resourceX, resourceY);
  text("Mana : "+str(playerMana), resourceX + 150, resourceY);
}
