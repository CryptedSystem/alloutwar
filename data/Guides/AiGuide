Ai files are written in lua and the following objects are available:

	unit 		| The unit executing the ai)
	path 		| The path to the data folder)
	Guard 		| The Guard Ai mode to use with the method unit:setCurrentAi(aiMode) )
	Pursuit 	| The Pursuit Ai mode)
	Move 		| The Move Ai mode)
	CarefulMove | The CarefulMove Ai mode)
	Autonomous 	| The Autonomous Ai mode)

The unit object can use the following methods 

	setMemory(index, value) | Assign a value to a case in the ai's memory 
	getMemory(index)		| Retrieve a value from the ai's memory (the memory is reset when changing AiMode)
	forward() 				| Move forward
	turn(angle)				| Rotate the unit
	attack(target)			| Attack a target which must be a Locable object
	getHP() 				| Retrieve the health of the unit
	getMaxHP() 				| Retrieve the maximum possible health of the unit
	getX() 					| Retrieve the x coordinate of the unit
	getY() 					| Retrieve the y coordinate of the unit
	getAngle()				| Retrieve the unit's rotation
	xTo(target)				| Compute the distance on the x-axis between the unit and the target which must be a Locable object
	yTo(target)				| Compute the distance on the y-axis between the unit and the target which must be a Locable object
	distanceTo(target)      | Compute the distance between the unit and the target which must be a Locable object
	angleTo(target)			| Compute the angle between the unit and the target which must be a Locable object
	nearestAllyUnit()		| Retrieve the nearest ally unit as a Locable object not including the unit 
	nearestEnemyUnit()		| Retrieve the nearest enemy unit as a Locable object, ignores environment
	nearestAllyBuilding     | Retrieve the nearest ally building as a Locable object
	nearestEnemyBuilding()  | Retrieve the nearest enemy building as a Locable object, ignores environment
	nearestAllyEntity()     | Retrieve the nearest ally entity (unit or building) as a Locable object
	nearestEnemyEntity()    | Retrieve the nearest enemy entity (unit or building) as a Locable object, ignores environment
	setDestination(target)	| Set a locable object as the target of the unit, target must be a Locable object
	getTarget()				| Retrieve the unit's target as a Locable object
	setCurrentAi(aiMode)	| Set the unit's Ai mode, must be one of the above Ai modes
	getCooldown()			| Retrieve the current state of the cooldown before the next attack
	getMaxCooldown()		| Retrieve the length of the cooldown when it's reset
	getSpeed() 				| Retrieve the unit's walking speed
	getDamage()				| Retrieve the damage dealt by a hit from the unit
	getRange()				| Retrieve the distance up to which the unit can attack
	getMinRange() 			| Retrieve the minimum distance a target must be at for the unit to attack it (always 0 for melee units)

The syntax to use one of the unit's methods is :

	units:methodName(parameter1, ... , parameterN)

You can include other scripts you've written by putting them inside a folder and starting your script with :
	
	package.path = package.path..";"..path.."/path/to/folder/myScript.lua"
	require("myScript")

	--with /path/to/folder/ being the path relative to the data folder and myScript.lua the name of the file
	--If you want to include multiple scripts from the same folder you can instead do :

	package.path = package.path..";"..path.."/path/to/folder/?.lua"
	require("myScript1")
	require("myScript2")
	...
	require("myScript1N")
