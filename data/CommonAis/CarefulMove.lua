package.path = package.path..";"..path.."/CommonAis/?.lua"
require(path.."/CommonAis/orientation")

enemy = unit:nearestEnemyEntity()
target = unit:getTarget()

function goToTarget(target)
	if target then
		if unit:distanceTo(target) > 10 then
			unit:turn(unit:angleTo(target) - unit:getAngle())
			unit:forward()
		else
			unit:setCurrentAi(Guard)
		end
	end
end

if enemy then
	if unit:distanceTo(enemy) < unit:getRange() and unit:distanceTo(enemy) > unit:getMinRange() then
		unit:turn(unit:angleTo(enemy) - unit:getAngle())
		unit:attack(enemy)
	elseif unit:distanceTo(enemy) < unit:getRange() + 100 then
		unit:turn(unit:angleTo(enemy) - unit:getAngle())
		unit:forward()
	elseif target then
		goToTarget(target)
	end
elseif target then
	goToTarget(target)
end