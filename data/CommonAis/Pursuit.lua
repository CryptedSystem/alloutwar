package.path = package.path..";"..path.."/CommonAis/?.lua"
require(path.."/CommonAis/orientation")

enemy = unit:getTarget()

if enemy then
	if unit:distanceTo(enemy) < unit:getRange() and unit:distanceTo(enemy) > unit:getMinRange() then
		unit:turn(unit:angleTo(enemy) - unit:getAngle())
		unit:attack(enemy)
	else
		unit:turn(unit:angleTo(enemy) - unit:getAngle())
		unit:forward()
	end
else
	unit:setCurrentAi(Guard)
end
