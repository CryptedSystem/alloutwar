package.path = package.path..";"..path.."/CommonAis/?.lua"
require(path.."/CommonAis/orientation")

target = unit:getTarget()

if unit:getMemory(0) == 0 then
	unit:setMemory(1,unit:getX())
	unit:setMemory(2,unit:getY())
	unit:setMemory(0,1)
elseif unit:getX() == unit:getMemory(1) and unit:getY() == unit:getMemory(2) then
	unit:setMemory(0, unit:getMemory(0) + 1)
else
	unit:setMemory(0,0)
end

if(unit:getMemory(0) > 30) then
	unit:setCurrentAi(Guard)
end

unit:setMemory(1,unit:getX())
unit:setMemory(2,unit:getY())

if target then
	if unit:distanceTo(target) > 10 then
		unit:turn(unit:angleTo(target) - unit:getAngle())
		unit:forward()
	else
		unit:setCurrentAi(Guard)
	end
end

