require "math"

function absTo(x)
	return x-unit:getX()
end

function ordTo(y)
	return y-unit:getY()
end

function distTo(x,y)
	return math.sqrt(absTo(x)*absTo(x)+ordTo(y)*ordTo(y))
end

function angTo(x,y)
	ang = math.acos(absTo(x)/distTo(x,y))
	if ordTo(y) < 0 then
		ang = - ang
	end
	return ang
end

function wander()
	if unit:getMemory(0) <= 0 then
		unit:setMemory(0,20)
		unit:turn(math.random(0,314)/100)
	end
	unit:setMemory(0,unit:getMemory(0)-1)
	unit:forward()
end