package.path = package.path..";"..path.."/CommonAis/?.lua"
require(path.."/CommonAis/orientation")

enemy = unit:nearestEnemyEntity()
if enemy then
	if unit:distanceTo(enemy) < unit:getRange() and unit:distanceTo(enemy) > unit:getMinRange() then
		unit:turn(unit:angleTo(enemy) - unit:getAngle())
		unit:attack(enemy)
	elseif unit:distanceTo(enemy) < unit:getRange() + 100 then
		unit:turn(unit:angleTo(enemy) - unit:getAngle())
		unit:forward()
	else
		wander()
	end
else
	wander()
end