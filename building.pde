//Fichier contenant tout ce qui relève des bâtiments

class Building extends Destroyable  //Classe gérant les bâtiments
{
  protected String name;			//Nom du bâtiment
  protected int ironCost;
  protected int manaCost;
  protected BoundedAnimation animation;	//Animation du bâtiment

  Building(float x, float y, float sizeX, float sizeY, int hp, BoundedAnimation animation, String team, String name, int ironCost, int manaCost) //Constructeur
  {
    super(x, y, sizeX, sizeY, hp, team);		//Appelle du contructeur de la classe mère
    this.name = name;						//Initialisation des attributs	
    this.ironCost = ironCost;
    this.manaCost = manaCost;	
    this.animation = animation;
  }

  Building(Building source) //Constructeur de copie
  {
    super(source.x, source.y, source.sizeX, source.sizeY, source.hp, source.team);		//Appelle du contructeur de la classe mère
    this.name = source.name;						//Initialisation des attributs	
    this.ironCost = source.ironCost;
    this.manaCost = source.manaCost;		
    this.animation = new BoundedAnimation(source.animation); //Copie de l'animation
  }

  void step()	//Méthode exécutée à chaque gérant les actions du bâtiment
  {
    //Non utilisée mais nécessaire pour le polymorphisme car utilisée par les classes filles
  }

  void disp()	//Méthode d'affichage
  {
    animation.disp(x, y);
    animation.animStep();
  }

  PImage getIcon() //Méthode récupérant l'image actuelle affichée par le bâtiment
  {
    return animation.getIcon();
  }

  Building cloneBuilding()  //Méthode retournant une copie du bâtiment, permet un clonage correct avec le polymorphisme
  {
    return new Building(this);
  }

  //Assesseurs et mutateurs 

  String getName()
  {
    return name;
  }

  int getManaCost()
  {
    return manaCost;
  }

  int getIronCost()
  {
    return ironCost;
  }
}

class DefenseBuilding extends Building //Classe gérant les bâtiments défensifs
{
  protected Projectile prj;				//Projectile tiré
  protected boolean thrown;				//Etat du projectile
  protected float minRange, maxRange;		//Distance minimale et maximale de tir
  protected int cooldown, maxCooldown;	//Temps de récupération entre chaque tir

  DefenseBuilding(float x, float y, float sizeX, float sizeY, float minRange, float maxRange, int cooldown, int hp, BoundedAnimation animation, String team, String name, Projectile prj, int ironCost, int manaCost) //Constructeur
  {
    super(x, y, sizeX, sizeY, hp, animation, team, name, ironCost, manaCost);	//Appel du constructeur de la classe mère
    this.prj = prj;									//Initialisation des attributs
    this.minRange = minRange;
    this.maxRange = maxRange;
    this.cooldown = 0;
    this.maxCooldown = cooldown;
  }

  DefenseBuilding(DefenseBuilding source) //Constructeur
  {
    super(source);	//Appel du constructeur de la classe mère
    this.prj = new Projectile(source.prj); //Initialisation des attributs
    this.minRange = source.minRange;
    this.maxRange = source.maxRange;
    this.cooldown = source.cooldown;
    this.maxCooldown = source.maxCooldown;
  }

  void attack(Locable target)	//Méthode d'attaque
  { 
    if (cooldown == 0)  //Si on a fini de recharger 
    {
      //Si l'on est pas en train de tirer, que le projectile n'est pas encore disparu et que la cible est à portée
      if (!thrown && distanceTo(target) <= maxRange && distanceTo(target) >= minRange ) 
      {
        prj.throwAt(x, y, angleTo(target), target);	//On lance le projectile
        thrown = true;		//On a tiré
        cooldown = maxCooldown; //On initie la recharge
      }
    }
  }

  void step()	//Méthode exécutée à chaque frame
  {
    super.step();	//On appelle la méthode de la classe mère
    if (nearestEnemyUnit() != null) //On attauque l'ennemi le plus proche
    {
      attack(nearestEnemyUnit());
    }
    if (thrown) //Si le projectile est tiré
    {
      prj.step();	//Il exécute son code
      if (prj.isDone() || distanceTo(prj) > maxRange) //S'il est trop loin ou s'il a touché
      {
        thrown = false;		//On réinitialise l'état
        prj.reset();			//On réinitialise le projectile
      }
    }
    if (cooldown > 0)	//Si la recharge est en cours
    {
      cooldown--;		//On décrémente le minuteur
    }
  }

  void disp()	//Méthode d'affichage
  {
    super.disp();	//Appel de la méthode de la classe mère
    if (thrown)	//Si le projectile est lancé
    {
      prj.disp();	//On l'affiche
    }
  }

  Building cloneBuilding() //Méthode de clonage du bâtiment
  {
    return new DefenseBuilding(this);
  }
}

class TrainingBuilding extends Building //Classe gérant les bâtiments de formation d'unités
{
  protected Unit[] produceable;			//Liste des unités pouvant être produites
  protected int[] producing;				//Liste des index des unités en production
  protected int countdown, maxCountdown;	//Minuteur de formation et temps de formation

  TrainingBuilding(float x, float y, float sizeX, float sizeY, int hp, int laneLength, int produceTime, Unit[] produceable, BoundedAnimation animation, String team, String name, int ironCost, int manaCost) //Constructeur
  {
    super(x, y, sizeX, sizeY, hp, animation, team, name, ironCost, manaCost);	//Appel du constructeur de la classe mère
    this.produceable = produceable; 				//Initialisation des attributs
    this.producing = new int[laneLength];
    this.maxCountdown = produceTime;
    this.countdown = produceTime;
    for (int i = 0; i < this.produceable.length; i++) //Mise à jour des attributs des unités produites
    {
      this.produceable[i].setX(this.x+this.sizeX+this.produceable[i].getSizeX());
      this.produceable[i].setY(this.y+this.sizeY+this.produceable[i].getSizeY());
      this.produceable[i].setTeam(this.team);
    }
    for (int i = 0; i < this.producing.length; i++) //Initialisation à vide de la file d'attente
    {
      this.producing[i] = -1;
    }
  }

  TrainingBuilding(TrainingBuilding source) //Constructeur
  {
    super(source);	//Appel du constructeur de la classe mère
    this.produceable = source.getUnitList(); 				//Initialisation des attributs
    this.producing = source.producing.clone();
    this.maxCountdown = source.maxCountdown;
    this.countdown = source.countdown;
  }

  //On modifie les méthodes héritées pour aussi changer les attributs des unités produites
  void setX(float x) 
  {
    super.setX(x);
    for (int i = 0; i < this.produceable.length; i++) //Mise à jour des positions des unités produites
    {
      this.produceable[i].setX(this.x+this.sizeX+this.produceable[i].getSizeX());
    }
  }

  void setY(float y)
  {
    super.setY(y);
    for (int i = 0; i < this.produceable.length; i++) //Mise à jour des positions des unités produites
    {
      this.produceable[i].setY(this.y+this.sizeY+this.produceable[i].getSizeY());
    }
  }

  void setTeam(String team)
  {
    super.setTeam(team);
    for (int i = 0; i < this.produceable.length; i++)
    {
      this.produceable[i].setTeam(this.team);
    }
  }

  boolean train(int id)	//Méthode ajoutant une unité à la file de formation 
  {
    for (int i = 0; i < producing.length; i++)	//On cherche la première position non occupée
    {
      if (producing[i] == -1)
      {
        producing[i] = id;	//On la remplace par l'index de l'unité à produire
        return true;		//On retourne vrai pour indiquer que l'unité sera formée
      }
    }

    return false;	//On retourne faux pour indiquer que la file d'attente est pleine
  }

  void step()	//Méthode exécutée à chaque frame
  {
    super.step();	//On appel la méthode de la classe mère

    if (producing[0] != -1)	//Si l'unité en production n'est pas vide
    {
      if (countdown <= 0)	//Si le minuteur est à 0 ou moins 
      {	
        units.add(produceable[producing[0]].cloneUnit()); //On produit l'unité
        countdown = maxCountdown;						  //On réinitialise le minuteur
        for (int i = 0; i < producing.length - 1; i++)	  //On décale les unités dans la file
        {
          producing[i] = producing[i+1];
        }
        producing[producing.length-1] = -1;				//On vide la dernière case de la fiel d'attente
      }	
      else 	//Sinon on décrémente le compteur
      {
        countdown--;
      }
    }
    else //Sinon on réinitialise le compteur
    {
      countdown = maxCountdown;
    }
  }

  Building cloneBuilding() //Méthode de clonage du bâtiment
  {
    return new TrainingBuilding(this);
  }

  Unit[] getUnitList() //Méthode récupérant une copie de la liste des unités que le bâtiment peut produire
  {
    Unit[] cloned = new Unit[produceable.length];
    for (int i = 0; i < cloned.length; i++)
    {
      cloned[i] = this.produceable[i].cloneUnit();
    }
    return cloned;
  }
}

enum Resource //Types de resources productibles
{
  Iron, Mana
}

class ResourceBuilding extends Building //Classe gérant les bâtiments produisant des ressources
{
  protected Resource resource;	//Type de ressource produite
  protected float amount;		//Quantité de ressource totale et quantité produite par cycle
  protected float rate;
  protected float mined;			//Quantité minée depuis la dernière récolte

  ResourceBuilding(float x, float y, float sizeX, float sizeY, int hp, Resource resource, int amount, float rate, BoundedAnimation animation, String team, String name, int ironCost, int manaCost)	//Constructeur
  {
    super(x, y, sizeX, sizeY, hp, animation, team, name, ironCost, manaCost);	//Appel du constructeur de la classe mère
    this.resource = resource;				//Initialisation des attributs
    this.amount = amount;
    this.rate = rate;
    this.mined = 0;
  }

  ResourceBuilding(ResourceBuilding source)	//Constructeur
  {
    super(source);	//Appel du constructeur de la classe mère
    this.resource = source.resource;				//Initialisation des attributs
    this.amount = source.amount;
    this.rate = source.rate;
    this.mined = source.mined;
  }

  void step()	//Méthode exécutée à chaque frame
  {
    super.step();	//Appel de la méthode de la classe mère
    mined += amount >= rate ? rate : amount; //On ajoute à la quantité minée la quantité produite à chaque cycle
    //Note : Ceci est un opérateur ternaire qui test une condition et assigne une valeur différente en fonction du résultat du test
    //Ici cela sert à empêcher de miner plus que ce qui est contenu dans le bâtiment
    amount = amount >= rate ? amount-rate : 0; //Un autre opérateur ternaire empéchant de tomber dans le négatif
  }

  int collect() //Méthode servant à collecter les ressources
  {
    int temp = (int)mined;
    mined -= temp;
    return temp;
  }

  int getMined()	//Assesseurs
  {
    return (int)mined;
  }

  Resource getResource()
  {
    return resource;
  }

  Building cloneBuilding()
  {
    return new ResourceBuilding(this);
  }
}

class Construction extends Building //Classe gérant un bâtiment en construction/d'un chantier
{
  Building futureBuilding;
  int timePassed;

  Construction(Building futureBuilding) //Constructeur
  {
    //Appel du constructeur de la classe mère
    super(futureBuilding.x, futureBuilding.y, futureBuilding.sizeX, futureBuilding.sizeY, futureBuilding.hp, loadBoundedAnimation("Buildings/construction/construction.anim"), futureBuilding.team, futureBuilding.name, futureBuilding.ironCost, futureBuilding.manaCost);
    this.futureBuilding = futureBuilding.cloneBuilding(); //Clonage du bâtiment à construire
    this.hp = 1;  //Initialisation des points de vie
  }

  void step() //Méthode exécutée à chaque frame
  {
    super.step();
    if (hp > 0) //Si le chantier n'est pas mort
    {
      hp ++;  //Ses points de vie augmentent
    }

    if (hp >= maxHP)  //Si il atteint ses points de vie maximaux la construction est finie
    {
      addBuilding(futureBuilding);  //On crée le bâtiment complet
      if (selectedBuilding == this)  //Si il est sélectionné on passe la sélection sur le bâtiment créé
      {
        selectedBuilding = futureBuilding;
      }
      this.hp = 0; //Il meurt
    }
  }
}
