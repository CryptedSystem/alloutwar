//Fichier contenant tout ce qui relève des contrôles clavier et souris

void renderSelected()   //Procédure affichant la sélection sur les entités sélectionnées et le rectangle de sélection
{
  if (selectedBuilding != null)
  {
    fill(0, 255, 0, 125);
    noStroke();
    rect(selectedBuilding.getX(), -selectedBuilding.getY(), selectedBuilding.getSizeX(), selectedBuilding.getSizeY());
  }

  for (int i = 0; i < selectedUnits.size(); i++)
  {
    fill(0, 255, 0, 125);
    noStroke();
    rect(selectedUnits.get(i).getX(), -selectedUnits.get(i).getY(), selectedUnits.get(i).getSizeX(), selectedUnits.get(i).getSizeY());
  }

  if (selecting)
  {
    fill(0, 255, 0, 125);
    noStroke();
    rect(sx1, -sy1, sx2-sx1, sy1-sy2);
  }
}

void showSelectedInfo() //Procédure affichant les infos sur la sélection
{
  if (selectedBuilding != null)
  {
    fill(0);
    stroke(0);
    PImage icon = selectedBuilding.getIcon();
    icon.resize(80, 80);
    image(icon, width/2, height-dimActionBtn-30);
    textAlign(LEFT);
    text("["+selectedBuilding.getName()+"]", width/2+80, height-dimActionBtn-10);
    text("Health : "+str(selectedBuilding.getHP())+"/"+str(selectedBuilding.getMaxHP()), width/2+80, height-dimActionBtn+10);
  }
  else if (selectedUnits.size() > 0)
  {
    fill(0);
    stroke(0);
    PImage icon = selectedUnits.get(0).getIcon();
    icon.resize(80, 80);
    image(icon, width/2, height-dimActionBtn-30);
    textAlign(LEFT);
    text("["+selectedUnits.get(0).getName()+"]", width/2+80, height-dimActionBtn-10);
    text("Health : "+str(selectedUnits.get(0).getHP())+"/"+str(selectedUnits.get(0).getMaxHP()), width/2+80, height-dimActionBtn+10);
  }
}

void mouseClicked() //Procédure gérant ce qui arrive en cas de clic de souris
{
  float menuBound = height-dimActionBtn-30; //Bord du menu
  if (mouseY < menuBound) //Si la souris est dans la zone de jeu
  {
    float mx = (mouseX-travelX)/scaleFactor;         //On calcule sa position dans le monde du jeu
    float my = -(mouseY-travelY)/scaleFactor;
    if (mouseButton == LEFT)  //Si c'est un clic gauche
    {
      if (placingBuilding)    //Si l'on est en train de placer un bâtiment
      {
        if (toPlace instanceof ResourceBuilding)  //Si c'est un bâtiment de ressources 
        {
          //On ne peut le placer que sur une source naturelle de cette ressource on regarde s'il y en a une sous la souris
          for (int i = 0; i < buildings.size(); i++)
          {
            if ( buildings.get(i).contains(mx, my) 
              && buildings.get(i) instanceof ResourceBuilding 
              && ((ResourceBuilding)buildings.get(i)).getResource() == ((ResourceBuilding)toPlace).getResource()
              && buildings.get(i).getTeam().equals("Environment") )
            {
              toPlace.setX(buildings.get(i).getX());
              toPlace.setY(buildings.get(i).getY());
              Building found = buildings.get(i);
              if (placeCurrentBuilding())
              {
                buildings.remove(found);
              }
              break;
            }
          }
        }
        else //Sinon on place juste le bâtiment 
        { 
          placeCurrentBuilding();
        }

        placingBuilding = false; //On sort du mode de construction
      }
      else if (!attackMode) //Sinon si l'on n'est pas en mode attaque
      {
        if (moveMode)   //Si l'on est en mode déplacement
        {
          setSelectionTarget(new Locable(mx, my));    //On place la destination des unités sur la souris
          setSelectionAi(AiMode.Move);              //On les fait passer en mode déplacement
          moveMode = false;                         //On sort du mode déplacement
        }
        else //Sinon 
        {
          resetSelectedBuilding();  //On réinitialise la sélection
          selectedUnits.clear();    
          for (int i = 0; i < buildings.size(); i++)  //On regarde si l'on a cliqué sur un bâtiment
          {
            if (buildings.get(i).contains(mx, my) && buildings.get(i).getTeam().equals(playerTeam))
            {
              selectedBuilding = buildings.get(i);
              selectedUnits.clear();
              break;
            }
          }

          if (selectedBuilding instanceof TrainingBuilding) //Si c'est un bâtiment de formation
          {
            //On génère les boutons de formation
            generateTrainingButtons(((TrainingBuilding)selectedBuilding).getUnitList(), (TrainingBuilding)selectedBuilding);
            training = true; //On passe en mode formation d'unités
          }
          else //Sinon on sort de ce mode
          {
            training = false;
          }
        }
      }
      else //Sinon on est en mode attaque
      {
        //On cherche si le joueur a visé une unité où un bâtiment si oui les unités sélectionnées le prendront pour cible
        //et passeront en mode attauque
        Locable target = null;
        for (int i = 0; i < units.size(); i++)
        {
          if (units.get(i).contains(mx, my))
          {
            target = (Locable)(units.get(i));
            setSelectionAi(AiMode.Pursuit);
            break;
          }
        }

        if (target == null)
        {
          for (int i = 0; i < buildings.size(); i++)
          {
            if (buildings.get(i).contains(mx, my))
            {
              target = (Locable)(buildings.get(i));
              setSelectionAi(AiMode.Pursuit);
              break;
            }
          }
        }

        if (target == null) //Si on n'a rien trouvé les unités prennent la zone comme destination et passe en déplacement prudent
        {
          target = new Locable(mx, my);
          setSelectionAi(AiMode.CarefulMove);
        }

        setSelectionTarget(target);
        toggleAttackMode();
      }
    }
    else if (mouseButton == RIGHT) //Sinon pour un clic droit
    {
      /*On cherche si le joueur a cliqué sur une entité ennemie et si oui les unités sélectionnées la prennent
       pour cible et passe en mode attaque, sinon elle prendront la zone cliquée comme destination et passeront
       en mode de déplacment non-prudent*/
      Locable target = null;
      placingBuilding = false;        //Permet d'annuler le placement d'un bâtiment
      for (int i = 0; i < units.size(); i++)
      {
        if (units.get(i).contains(mx, my) && !units.get(i).getTeam().equals(playerTeam) && !units.get(i).getTeam().equals("Environment"))
        {
          target = (Locable)(units.get(i));
          setSelectionAi(AiMode.Pursuit);
          break;
        }
      }

      if (target == null)
      {
        for (int i = 0; i < buildings.size(); i++)
        {
          if (buildings.get(i).contains(mx, my) && !buildings.get(i).getTeam().equals(playerTeam) && !buildings.get(i).getTeam().equals("Environment"))
          {
            target = (Locable)(buildings.get(i));
            setSelectionAi(AiMode.Pursuit);
            break;
          }
        }
      }

      if (target == null)
      {
        target = new Locable(mx, my);
        setSelectionAi(AiMode.Move);
      }

      setSelectionTarget(target);
    }
  }
} 

void mousePressed() //Procédure appelée quand la souris est pressée
{
  float mx = (mouseX-travelX)/scaleFactor;           //Calcul de la position de la souris dans le monde du jeu
  float my = -(mouseY-travelY)/scaleFactor;
  if (mouseButton == LEFT) // En cas de clic gauche 
  {
    if (!attackMode && ! placingBuilding) //Si l'on ne place pas un bâtiment et si l'on n'est pas en mode attaque
    {
      //On place les coordonnées du premier coin rectangle de sélection sur la souris
      sx1 = mx;
      sy1 = my;
    }
  }
}

void mouseDragged() //Procédure appelée quand la souris est déplacée avec un bouton enfoncé
{
  if (mouseButton == LEFT)  //Si le bouton gauche est enfoncé
  {
    if (!attackMode && !placingBuilding)  //Si l'on n'est ni en mode attaque ni en mode construction
    {
      float mx = (mouseX-travelX)/scaleFactor;           //On calcule la position de la souris dans le monde du jeu
      float my = -(mouseY-travelY)/scaleFactor;
      sx2 = mx;   //On place Le deuxième coin du rectangle de sélection sur la souris
      sy2 = my;
      selecting = true; //On passe en mode de sélection
    }
  }
}

void mouseReleased()  //Procédure appelée lorsque d'un bouton de la souris est relaché
{
  if (mouseButton == LEFT && !attackMode && selecting) //Si c'est le bouton gauche et que l'on sélectionnait
  {
    //On s'assure de le deuxième coin a des coordonnées supérieures à celles du premier sinon on les interchange
    if (sx1 > sx2)
    {
      sx1 += sx2;
      sx2 = sx1 - sx2;
      sx1 -= sx2;
    }
    if (sy1 < sy2)
    {
      sy1 += sy2;
      sy2 = sy1 - sy2;
      sy1 -= sy2;
    }

    //On crée un objet Destroyable pour profiter de sa méthode collide(...)
    Destroyable selection = new Destroyable(sx1, sy1, sx2-sx1, sy1-sy2, 15, "Gui");

    //On réinitialise la sélection
    selectedUnits.clear();
    resetSelectedBuilding();

    //On teste les collisions avec des unités de l'équipe du joueur qu'on ajoute alors à la liste des unités sélectionnées
    for (int i = 0; i < units.size(); i++)
    {
      if (selection.collide(units.get(i)) && units.get(i).getTeam().equals(playerTeam))
      {
        selectedUnits.add(units.get(i));
      }
    }
    selecting = false; //On sort du mode de sélection
  }
}

void mouseWheel(MouseEvent event) //Procédure appelée quand la roue de souris est tournée
{
  float c_x = (width/2-travelX)/scaleFactor;
  float c_y = (height/2-travelY)/scaleFactor;
  scaleFactor *= pow(1.1,-event.getCount()); //On gère alors le zoom

  //Formules obtenues par résolution d'équations pour zoomer sur le centre
  travelX = width/2 - scaleFactor * c_x;   
  travelY = height/2 - scaleFactor * c_y;
}

void keyPressed() //Procédure appelée quand une touche du clavier est enfoncée
{
  if (keyCode == ESC)   //La touche Echap amène le menu en jeu
  { 
    key = 0;
    displayInGameMenu();
  }
  else if (key == 'a')    //La touche a active le mode d'attaque
  {
    toggleAttackMode();
  }
  else if (key == 'o')  //Le touche o active le mode autonome des unités
  {
    setSelectionAi(AiMode.Autonomous);
  }
  else if (key == 's')  //La touche s passe lesunités en mode garde
  {
    for (int i = 0; i < selectedUnits.size(); i++)
    {
      setSelectionAi(AiMode.Guard);
    }
  }
}

void setSelectionAi(AiMode mode)  //Procédure mettant à jour l'IA de toutes les unités sélectionnées
{
  for (int i = 0; i < selectedUnits.size(); i++)
  {
    selectedUnits.get(i).setCurrentAi(mode);
  }
}

void setSelectionTarget(Locable target) //Procédure mettant à jour la cible/destination de toutes les unités sélectionnées
{
  for (int i = 0; i < selectedUnits.size(); i++)
  {
    if (selectedUnits.get(i) != target)
    {
      selectedUnits.get(i).setTarget(target);
    }
  }
}

void toggleAttackMode() //Procédure activant et désactivant le mode attaque
{
  attackMode = !attackMode;
  if (attackMode)
  {
    cursor(CROSS);
    moveMode = false;
  }
  else 
  {
    cursor(ARROW);
  }
}

void toggleMoveMode() //Procédure activant et désactivant le mode déplacement
{
  moveMode = !moveMode;
  if (moveMode && attackMode)
  {
    toggleAttackMode();
  }
}

void renderPlacing()  //Procédure affichant le bâtiment à placer en mode construction
{
  float mx = (mouseX-travelX)/scaleFactor; 
  float my = -(mouseY-travelY)/scaleFactor;
  toPlace.setX(mx);
  toPlace.setY(my);
  toPlace.disp();
}

void resetSelectedBuilding()  //Procédure réinitialisant le bâtiment sélectionné
{
  selectedBuilding = null;
  training = false;
}

boolean placeCurrentBuilding()  //Fonction lançant la construction d'un bâtiment sélectionné et retournant si ce fût un succès
{
  Building nearest = (Building)toPlace.nearestAllyBuilding(false);
  if(nearest != null)
  {
    Locable adjusted = new Locable(nearest.getX() + nearest.getSizeX()/2, nearest.getY() - nearest.getSizeY()/2);
    if (playerMana > toPlace.getManaCost() && playerIron > toPlace.getIronCost() && toPlace.distanceTo(adjusted) < buildRadius)
    {
      addBuilding(new Construction(toPlace));
      playerMana -= toPlace.getManaCost();
      playerIron -= toPlace.getIronCost();
      return true;
    }
    return false;
  }
  return false;
}

void handleTravel() //Procédure gérant le déplacment de la vue sur la carte
{
  float moveSensibility = 30;
  stroke(255,255,255,100);
  fill(255,255,255,100);
  if(mouseY > height-moveSensibility)
  {
    travelY -= 10;
    rect(0,height-moveSensibility,width,moveSensibility);
  }
  else if (mouseY < moveSensibility) 
  {
    travelY += 10;
    rect(0,0,width,moveSensibility);
  }
  if(mouseX > width-moveSensibility)
  {
    travelX -= 10;
    rect(width-moveSensibility,0,width,height);
  }
  else if (mouseX < moveSensibility) 
  {
    travelX += 10;
    rect(0,0,moveSensibility,height);
  }
}
