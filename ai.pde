//Fichier dans lequel les classes et fonctions gérant les ia seront définies

class Ai //Classe gérant les ia
{
  protected Globals globals; //L'environnement du script
  protected LuaValue chunk; //Stock le script sous sa forme exécutable
  protected float[] memory;	//Mémoire de l'ia
  protected String filepath;//Chemin de l'ia
  Ai(String filepath)	//Constructeur
  {
    memory = new float[10];		//Initialisation de la mémoire
    clearMemory();
    globals = JsePlatform.standardGlobals(); //On crée l'environnement du script
    chunk = globals.loadfile(dataPath("")+"/"+filepath); //Chargement du script
    globals.set("path", dataPath("")); //On donne à l'ia le chemin des ressources du programme pour les imports d'autres fichiers lua par exemple
    if (new File(dataPath(filepath)).isFile()) //Si le fichier existe
    {
      this.filepath = filepath; //On stocke son chemin
    }
    else //Sinon 
    {
      //On informe d'une erreur et on charge un fichier par défaut
      print("Error, the file : " + filepath + " was not found, loading an error file instead");
      this.filepath = "Error/Error.lua";
    }
  }

  Ai(Ai source) //Constructeur de copie
  {
    this.memory = source.memory.clone();	//Copie des attributs
    this.globals = JsePlatform.standardGlobals(); //Création d'un nouvel environnement pour le script
    this.filepath = source.filepath;  //Copie du chemin
    this.chunk = globals.loadfile(dataPath("")+"/"+source.filepath);	//Rechargement du script
    globals.set("path", dataPath(""));		//Ajout du chemin du script aux variables accessibles par celui-ci
  }

  void step() //Méthode gérant l'appel de l'ia
  {
    try //On essaie de faire tourner le script
    {
      chunk.call();
    }
    catch(LuaError e) //En cas d'erreur il s'arrête et une erreur est affichée
    {
      print("Error : AI "+filepath+" has enountered an error, it's code won't be executed.\n");
      print("Exact Error is : "+e.toString()+"\n");
    }
  }	

  void clearMemory()	//Méthode nettoyant la mémoire de l'ia
  {
    for (int i = 0; i < memory.length; i++)
    {
      memory[i] = 0;
    }
  }
}

class UnitAi extends Ai //Classe gérant les ia d'unités
{
  Unit body; //Unité contrôlée par l'ia

  UnitAi(String filepath, Unit body) //Constructeur
  {
    super(filepath);	//Appel du constructeur de la classe mère
    this.body = body;	//Initialisation des attributs
    globals.set("unit", CoerceJavaToLua.coerce(this)); //Ajout du corps à la liste des objets accesisbles par le script lua	
    globals.set("Guard", CoerceJavaToLua.coerce(AiMode.Guard));
    globals.set("Pursuit", CoerceJavaToLua.coerce(AiMode.Pursuit));
    globals.set("Move", CoerceJavaToLua.coerce(AiMode.Move));
    globals.set("CarefulMove", CoerceJavaToLua.coerce(AiMode.CarefulMove));
    globals.set("Autonomous", CoerceJavaToLua.coerce(AiMode.Autonomous));
  }

  UnitAi(UnitAi source, Unit body) //Constructeur de copie
  {
    super(source);		//Appel du constructeur de la classe mère
    this.body = body;	//Initialisation de l'attribut body, celui ci n'est pas copié car la copie d'une unité est une nouvelle unité, on ne veut pas controler l'ancienne avec cette ia
    globals.set("unit", CoerceJavaToLua.coerce(this)); //Ajout du corps à la liste des objets accesisbles par le script lua
    globals.set("Guard", CoerceJavaToLua.coerce(AiMode.Guard));
    globals.set("Pursuit", CoerceJavaToLua.coerce(AiMode.Pursuit));
    globals.set("Move", CoerceJavaToLua.coerce(AiMode.Move));
    globals.set("CarefulMove", CoerceJavaToLua.coerce(AiMode.CarefulMove));
    globals.set("Autonomous", CoerceJavaToLua.coerce(AiMode.Autonomous));
  }

  //Assesseurs et mutateurs, en englobant les méthodes ici on empêche l'ia d'accéder à des méthodes non autorisées sur l'unité

  protected void setBody(Unit body)
  {
    this.body = body;
  }

  void setMemory(int index, float value)
  {
    memory[index] = value;
  }

  float getMemory(int index)
  {
    return memory[index];
  }

  void forward()
  {
    body.forward();
  }

  void turn(float amount)
  {
    body.turn(amount);
  }

  void attack(Locable target)
  {
    body.attack(target);
  }

  int getHP()
  {
    return body.getHP();
  }

  int getMaxHP()
  {
    return body.getMaxHP();
  }
  float getX()
  {
    return body.getX();
  }

  float getAngle()
  {
    return body.getAngle();
  }

  float getY()
  {
    return body.getY();
  }

  float xTo(Locable other)	//Fonction calculant la distance en abscisse à un autre objets Locable
  {
    return body.xTo(other);
  }

  float yTo(Locable other)   //Fonction calculant la distance en ordonnée à un autre objets Locable
  {
    return body.yTo(other);
  }

  float distanceTo(Locable other) //Fonction calculant la distance à un autre objets Locable
  { 
    return body.distanceTo(other);
  }

  float angleTo(Locable other)   //Fonction donnant l'angle entre la droite passant par l'objet et un autre et l'axe des abscisses
  {
    return body.angleTo(other);
  }

  Locable nearestAllyUnit()
  {
    return body.nearestAllyUnit();
  }

  Locable nearestEnemyUnit()
  {
    Locable nearest = body.nearestEnemyUnit();
    if(distanceTo(nearest) < sightRadius)
    {
      return nearest;
    }
    return null;
  }

  Locable nearestAllyBuilding()
  {
    return body.nearestAllyBuilding();
  }

  Locable nearestEnemyBuilding()
  {
    Locable nearest = body.nearestEnemyBuilding();
    if(distanceTo(nearest) < sightRadius)
    {
      return nearest;
    }
    return null;
  }

  Locable nearestAllyEntity()
  {
    return body.nearestAllyEntity();
  }

  Locable nearestEnemyEntity()
  {
    Locable nearest = body.nearestEnemyEntity();
    if(distanceTo(nearest) < sightRadius)
    {
      return nearest;
    }
    return null;
  }

  void setDestination(Locable destination)
  {
    body.setDestination(destination);
  }

  Locable getDestination()
  {
    return body.getDestination();
  }

  void setTarget(Locable target)
  {
    body.setTarget(target);
  }

  Locable getTarget()
  {
    return body.getTarget();
  }

  void setCurrentAi(AiMode mode)
  {
    body.setCurrentAi(mode);
  }

  float getCooldown()
  {
    return body.getCooldown();
  }

  float getMaxCooldown()
  {
    return body.getMaxCooldown();
  }

  float getSpeed()
  {
    return body.getSpeed();
  }

  float getDamage()
  {
    return body.getDamage();
  }

  float getRange()
  {
    return body.getRange();
  }

  float getMinRange()
  {
    if (body instanceof RangeUnit)
    {
      return ((RangeUnit)body).getMinRange();
    }
    return 0;
  }
}
