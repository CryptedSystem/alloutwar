//Fichier dans lequel les classes des unités seront définies 

class Locable	//Classe d'objets possédant une position, en faire hériter lesautres classes permet de généraliser les fonctions de localisation
{
  protected float x, y;	//Position

  Locable(float x, float y)	//Constructeur
  {
    this.x = x;
    this.y = y;
  }
  //Mutateurs protégés pour empêcher una IA de les utiliser 

  protected void setX(float x)
  {
    this.x = x;
  }

  protected void setY(float y)
  {
    this.y = y;
  }

  //Assesseurs
  float getX()
  {
    return x;
  }

  float getY()
  {
    return y;
  }

  float xTo(Locable other)	//Méthode calculant la distance en abscisse à un autre objets Locable
  {
    return other.getX() - x;
  }

  float yTo(Locable other)   //Méthode calculant la distance en ordonnée à un autre objets Locable
  {
    return other.getY()-y;
  }

  float distanceTo(Locable other) //Méthode calculant la distance à un autre objets Locable
  { 
    return sqrt(pow(xTo(other), 2)+pow(yTo(other), 2));
  }

  float angleTo(Locable other)   //Méthode donnant l'angle entre la droite passant pat l'objet et un autre et l'axe des abscisses
  {
    float ang = acos(xTo(other)/distanceTo(other));	//On prend une mesure possible de l'angle depuis son cosinus
    if (yTo(other) < 0)	//Si l'écart en ordonnée est négatif, le sinus l'est aussi et l'angle est l'opposé de celui calculé
    {
      ang = -ang;
    }
    return ang;
  }
}

class Destroyable extends Locable	//Classe d'objets pouvant être détruits, permet de généraliser les fonctions impliquant des points de vie
{
  protected int hp, maxHP;		//Points de vie actuels et maximaux
  protected String team;			//Equipe
  protected float sizeX, sizeY; //Taille

  Destroyable(float x, float y, float sizeX, float sizeY, int hp, String team)	//Constructeur
  {
    super(x, y);			//Appel du constructeur de la classe mère
    this.hp = hp;		//Initialisation des attributs
    this.maxHP = hp;
    this.team = team;
    this.sizeX = sizeX;
    this.sizeY = sizeY;
  }

  //Mutateur 

  void setTeam(String team)
  {
    this.team = team;
  }

  //Assesseurs

  int getHP()		
  {
    return hp;
  }

  int getMaxHP()		
  {
    return maxHP;
  }

  float getSizeX()
  {
    return sizeX;
  }

  float getSizeY()
  {
    return sizeY;
  }

  String getTeam()
  {
    return team;
  }

  void reduceHP(int amount) //Méthode pour perdre des points de vie
  {
    hp = max(hp-amount, 0);	//On ne peut pas descendre en dessous de 0
  }

  boolean contains(float x, float y)
  {
    return (x > this.x && x < this.x + this.sizeX && y < this.y && y > this.y - this.sizeY);
  }

  boolean contains(Destroyable other)
  {
    return (other.getX() > x && other.getX()+other.getSizeX() < x + sizeX && other.getY() < y && other.getY()-other.getSizeY() > y - sizeY);
  }

  boolean collide(Destroyable other)
  {
    return (x < (other.getX() + other.getSizeX()) && (x + getSizeX()) > other.getX() && y > (other.getY() - other.getSizeY()) && (y - getSizeY()) < other.getY());
  }

  Locable nearestAllyUnit() //Méthode pour trouver l'unité alliée la plus proche
  {
    int index = -1;			//On donne des valeurs par défauts aux arguments
    float dist = 999999999;	//On initialise la distance minimum à un nombre bien supérieur au possible
    for (int i = 0; i < units.size(); i++)	//On cherche dans toutes les unités
    {
      if (this != units.get(i) && distanceTo(units.get(i)) < dist && units.get(i).getTeam().equals(team))	//Si une est plus proche que le record
      {
        index = i;						//On met à jour le record
        dist = distanceTo(units.get(i));
      }
    }
    if (index >= 0)	//Si une unité a été trouvée
    {
      return (Locable)units.get(index);	//On retourne celle-ci sous forme d'un Locable pour empécher l'accès à ses fonctions gérant les pv et autre
    }
    return null; //Sinon on retourne un pointeur nul
  }

  Locable nearestEnemyUnit() //Méthode trouvant l'unité ennemie la plus proche
  {
    //Fonctionnement très similaire à la fonction précédente
    int index = -1;		
    float dist = 999999999;
    for (int i = 0; i < units.size(); i++)
    {
      if (distanceTo(units.get(i)) < dist && !units.get(i).getTeam().equals(team) && !units.get(i).getTeam().equals("Environment"))
      {
        index = i;
        dist = distanceTo(units.get(i));
      }
    }
    if (index >= 0)
    {
      return (Locable)units.get(index);
    }
    return null;
  }

  Locable nearestAllyBuilding() //Méthode pour trouver l'unité alliée la plus proche
  {
    return nearestAllyBuilding(true);
  }

  Locable nearestAllyBuilding(boolean includeConstructions) //Méthode pour trouver l'unité alliée la plus proche
  {
    int index = -1;			//On donne des valeurs par défauts aux arguments
    float dist = 999999999;	//On initialise la distance minimum à un nombre bien supérieur au possible
    for (int i = 0; i < buildings.size(); i++)	//On cherche dans toutes les unités
    {
      if (this != buildings.get(i) &&
        distanceTo(buildings.get(i)) < dist &&
        buildings.get(i).getTeam().equals(team) &&
        (includeConstructions || !(buildings.get(i) instanceof Construction)) )	//Si une est plus proche que le record
      {
        index = i;						//On met à jour le record
        dist = distanceTo(buildings.get(i));
      }
    }
    if (index >= 0)	//Si une unité a été trouvée
    {
      return (Locable)buildings.get(index);	//On retourne celle-ci sous forme d'un Locable pour empécher l'accès à ses fonctions gérant les pv et autre
    }
    return null; //Sinon on retourne un pointeur nul
  }

  Locable nearestEnemyBuilding() //Méthode trouvant l'unité ennemie la plus proche
  {
    //Fonctionnement très similaire à la fonction précédente
    int index = -1;		
    float dist = 999999999;
    for (int i = 0; i < buildings.size(); i++)
    {
      if (distanceTo(buildings.get(i)) < dist && !buildings.get(i).getTeam().equals(team) && !buildings.get(i).getTeam().equals("Environment"))
      {
        index = i;
        dist = distanceTo(buildings.get(i));
      }
    }
    if (index >= 0)
    {
      return (Locable)buildings.get(index);
    }
    return null;
  }

  Locable nearestEnemyEntity() //Méthode trouvant l'entité ennemie la plus proche
  {
    if (nearestEnemyUnit() != null)
    {
      if (nearestEnemyBuilding() != null)
      {
        if (distanceTo(nearestEnemyUnit()) <= distanceTo(nearestEnemyBuilding()))
        {
          return nearestEnemyUnit();
        }

        return nearestEnemyBuilding();
      }
      return nearestEnemyUnit();
    }
    if (nearestEnemyBuilding() != null)
    {
      return nearestEnemyBuilding();
    }
    return null;
  }

  Locable nearestAllyEntity() //Méthode trouvant l'entité ennemie la plus proche
  {
    if (nearestAllyUnit() != null)
    {
      if (nearestAllyBuilding() != null)
      {
        if (distanceTo(nearestAllyUnit()) <= distanceTo(nearestAllyBuilding()))
        {
          return nearestAllyUnit();
        }

        return nearestAllyBuilding();
      }
      return nearestAllyUnit();
    }
    if (nearestAllyBuilding() != null)
    {
      return nearestAllyBuilding();
    }
    return null;
  }
}

enum Direction	//Enum listant les directions possibles du sprite, facilite la gestion des animations plus loin
{
  Down, Up, Left, Right;
}

enum AiMode	//Types d'IA possibles
{
  Guard, Pursuit, Move, CarefulMove, Autonomous
}

class Unit extends Destroyable	//Classe unité (de mélée)
{
  protected AnimationHandler animator;	//Gestionnaire d'animations, contient les animations dans un ordre standardisé
  protected float speed;					//Vitesse
  protected int damage; 					//Dommages causées par une attaque
  protected int cooldown, maxCooldown;	//Temps depuis la dernière attauque et entre chaque attaque 
  protected float angle;					//Angle d'orientation
  protected float range;					//Portée d'une attaque
  protected Direction facing;				//Direction du sprite
  protected String name, race;			//Nom de l'unité et race
  protected UnitAi[] ais;					//Les diverses ia
  protected AiMode currentAi;				//L'ia actuelle
  protected Locable target;				//La cible dans le cas d'une poursuite
  protected Locable destination;
  protected int offX, offY;
  protected int ironCost, manaCost;
  protected boolean hasMoved;

  Unit(float x, float y, float sizeX, float sizeY, float speed, int hp, float range, int cooldown, int damage, String name, String race, String team, AnimationHandler animator) //Constructeur
  {
    super(x, y, sizeX, sizeY, hp, team);				//Appel du constructeur de la classe mère
    this.speed = speed; 		//Initialisation des attributs
    this.animator = animator;
    this.angle = -HALF_PI;
    this.damage = damage;
    this.cooldown = 0;
    this.maxCooldown = cooldown;
    this.range = range;
    this.name = name;
    this.team = team;
    this.race = race;
    this.facing = Direction.Down;
    this.currentAi = AiMode.Guard;
    this.offX = 0;
    this.offY = 0;
    this.ironCost = 0;
    this.manaCost = 0;
    this.hasMoved = false;
  }

  Unit(float x, float y, float sizeX, float sizeY, float speed, int hp, float range, int cooldown, int damage, String name, String race, String team, AnimationHandler animator, int offX, int offY, int ironCost, int manaCost) //Constructeur
  {
    this(x, y, sizeX, sizeY, speed, hp, range, cooldown, damage, name, race, team, animator);
    this.offX = offX;
    this.offY = offY;
    this.ironCost = ironCost;
    this.manaCost = manaCost;
  }

  Unit(Unit source) //Constructeur de copie
  {
    super(source.x, source.y, source.sizeX, source.sizeY, source.hp, source.team);	//Appel du constructeur de la classe mère
    this.speed = source.speed; 		//Copie des attributs
    this.animator = new AnimationHandler(source.animator);
    this.angle = source.angle;
    this.damage = source.damage;
    this.range = source.range;
    this.name = source.name;
    this.race = source.race;
    this.facing = source.facing;
    this.currentAi = source.currentAi;
    this.offX = source.offX;
    this.offY = source.offY;
    this.ironCost = source.ironCost;
    this.manaCost = source.manaCost;
    this.ais = new UnitAi[source.ais.length];
    this.hasMoved = source.hasMoved;
    for (int i = 0; i < source.ais.length; i++)
    {
      this.ais[i] = new UnitAi(source.ais[i], this);
      this.ais[i].clearMemory();
    }
  }

  Unit cloneUnit()	//Méthode de clonage, permet de respecter le polymorphisme
  {
    return new Unit(this);
  }

  //Mutateurs

  void setAis(UnitAi[] ais)
  {
    this.ais = ais;
  }

  void setCurrentAi(AiMode mode)
  {
    this.currentAi = mode;
    this.ais[this.currentAi.ordinal()].clearMemory();
  }

  void setTarget(Locable target)
  {
    this.target = target;
  }

  void setDestination(Locable destination)
  {
    this.destination = destination;
  }

  //Assesseurs

  float getCooldown()
  {
    return cooldown;
  }

  float getMaxCooldown()
  {
    return maxCooldown;
  }

  float getSpeed()
  {
    return speed;
  }

  float getDamage()
  {
    return damage;
  }

  float getRange()
  {
    return range;
  }

  float getAngle()  
  {
    return angle;
  }

  String getRace()
  {
    return race;
  }

  String getName()
  {
    return name;
  }

  Locable getTarget()
  {
    return target;
  }

  Locable getDestination()
  {
    return destination;
  }

  int getIronCost()
  {
    return ironCost;
  }

  int getManaCost()
  {
    return manaCost;
  }

  PImage getIcon()
  {
    return animator.getIcon();
  }

  void attack(Locable target)	//Méthode d'attaque
  {
    if (cooldown == 0)	//Si le minuteur est à 0
    {
      if (distanceTo(target) <= range && animator.getCurrentAnimation() != 8 + facing.ordinal() && target != this)		//Si la cible est à portée on peut attaquer
      {
        animator.playAnimation(8 + facing.ordinal(), 1); //On joue l'animation d'attaque (La première est à l'index 8 et sont rangées dans l'odre des éléments de l'enum)
        if (target instanceof Destroyable)
        {
          if (((Destroyable)target).getHP() > 0)
          {
            ((Destroyable)target).reduceHP(damage);		 //On retire les points de vie à la cible
          }
          else 
          {
            target = null;
            setCurrentAi(AiMode.Guard);
          }
        }
        cooldown = maxCooldown;
      }
    }
  }

  void forward()				//Méthode pour avancer
  {
    if (!hasMoved) //Empêche deux déplacments en une frame
    {
      float nx = x + speed * cos(angle);	
      float ny = y;
      boolean move = true;
      nx = min(max(WEST, nx), EAST);	//On s'assure de rester dans la fenêtre (à modifier par un test des limites de la map)


      Destroyable n_pos = new Destroyable(nx, ny, sizeX, sizeY, hp, team);

      for (int i = 0; i < buildings.size() && move; i++)	//On test si on entre en collision avec un bâtiment
      {
        if (n_pos.collide(buildings.get(i)))	//Si oui on retourne à l'ancienne position
        {
          move = false;
        }
      }
      for (int i = 0; i < units.size() && move; i++)	//On test si on entre en collision avec un bâtiment
      {
        if (units.get(i) != this && n_pos.collide(units.get(i)))	//Si oui on retourne à l'ancienne position
        {
          move = false;
        }
      }

      if (!move)
      {
        nx = x;
      }

      ny = y + speed * sin(angle);
      ny = min(max(SOUTH, ny), NORTH);
      n_pos = new Destroyable(nx, ny, sizeX, sizeY, hp, team);
      move = true;

      for (int i = 0; i < buildings.size() && move; i++)	//On test si on entre en collision avec un bâtiment
      {
        if (n_pos.collide(buildings.get(i)))	//Si oui on retourne à l'ancienne position
        {
          move = false;
        }
      }
      for (int i = 0; i < units.size() && move; i++)	//On test si on entre en collision avec un bâtiment
      {
        if (units.get(i) != this && n_pos.collide(units.get(i)))	//Si oui on retourne à l'ancienne position
        {
          move = false;
        }
      }

      if (!move)
      {
        ny = y;
      }

      x = nx;
      y = ny;

      animator.playAnimation(4 + facing.ordinal(), 1); //On joue l'animation (première animation de marche à l'index 4)
      hasMoved = true;
    }
  }

  void turn(float amount)	//Méthode pour tourner
  {
    angle += amount;			//On met à jour l'angle
    if ( abs(sin(angle)) >= abs(cos(angle)) )	//En fonction de la direction générale de l'angle on met à jour la direction du sprite
    {
      if (sin(angle) < 0)
      {
        facing = Direction.Down;
      }
      else
      {
        facing = Direction.Up;
      }
    }
    else
    {
      if (cos(angle) < 0)
      {
        facing = Direction.Left;
      }
      else
      {
        facing = Direction.Right;
      }
    }

    animator.setDefaultAnimation(0 + facing.ordinal()); //On met  àjour l'animation de repos
  }

  void step() //Méthode exécutée à chaque frame
  {
    /*La collision est mitigée par le fait que si un déplacement est possible purement en abscisse ou en ordonnée il
     est effectué*/
    hasMoved = false;
    for (int i = 0; i < buildings.size(); i++)	//On test si on entre en collision avec un bâtiment
    {
      if (collide(buildings.get(i)))	//Si oui on retourne à l'ancienne position
      {
        float d_x = x - buildings.get(i).getX();
        float d_y = y - buildings.get(i).getY();

        while (d_x == 0)
        {
          d_x = random(-1, 1);
        }

        while (d_y == 0)
        {
          d_y = random(-1, 1);
        }

        d_x = d_x / abs(d_x);
        d_y = d_y / abs(d_y);


        x += d_x;
        y += d_y;
      }
    }
    for (int i = 0; i < units.size(); i++)	//On test si on entre en collision avec un bâtiment
    {
      if (units.get(i) != this && collide(units.get(i)))	//Si oui on retourne à l'ancienne position
      {
        float d_x = x - units.get(i).getX();
        float d_y = y - units.get(i).getY();

        while (d_x == 0)
        {
          d_x = random(-1, 1);
        }

        while (d_y == 0)
        {
          d_y = random(-1, 1);
        }

        d_x = d_x / abs(d_x);
        d_y = d_y / abs(d_y);


        x += d_x;
        y += d_y;
      }
    }
    if (cooldown > 0)
      cooldown--;
    ais[currentAi.ordinal()].step();
  }

  void disp()	//Méthode d'affichage
  {
    animator.disp(x-offX, y+offY); //On affiche l'animation aux coordonnées
    stroke(255, 0, 0);
    noFill();
    animator.step();	//On la fait progresser d'une frame
  }
}

class RangeUnit extends Unit //Classe pour les unités à distance
{
  protected float minRange;		//Portée minimale du tir, il est impossible de tirer trop près
  protected boolean throwing, thrown;	//Booléen gérant la phase du tir dans laquelle on est
  protected Projectile pj;		//Projectile tiré

  RangeUnit(float x, float y, float sizeX, float sizeY, float speed, int hp, float minRange, float maxRange, int cooldown, String name, String race, String team, Projectile pj, AnimationHandler animator) //Constructeur
  {
    super(x, y, sizeX, sizeY, speed, hp, maxRange, cooldown, 0, name, race, team, animator); //On appelle le constructeur de la classe mère
    this.minRange = minRange;		//On initialise les attributs
    thrown = false;
    throwing = false;
    this.pj = pj;
  }
  RangeUnit(float x, float y, float sizeX, float sizeY, float speed, int hp, float minRange, float maxRange, int cooldown, String name, String race, String team, Projectile pj, AnimationHandler animator, int offX, int offY, int ironCost, int manaCost) //Constructeur
  {
    super(x, y, sizeX, sizeY, speed, hp, maxRange, cooldown, 0, name, race, team, animator, offX, offY, ironCost, manaCost); //On appelle le constructeur de la classe mère
    this.minRange = minRange;		//On initialise les attributs
    thrown = false;
    throwing = false;
    this.pj = pj;
  }

  RangeUnit(RangeUnit source) //Constructeur de copie
  {
    super(source);
    this.pj = new Projectile(source.pj);
    this.thrown = source.thrown;
    this.throwing = source.throwing;
  }

  Unit cloneUnit()
  {
    return (Unit)(new RangeUnit(this));
  }

  void attack(Locable target)	//Méthode d'attaque
  {
    if (cooldown == 0)
    {
      if (target instanceof Destroyable && !throwing && !thrown && distanceTo(target) <= range && distanceTo(target) >= minRange && target != this) //Si l'on est pas en train de tirer, que le projectile n'est pas encore disparu et que la cible est à portée
      {
        if (((Destroyable)target).getHP() > 0)
        {
          animator.playAnimation(8 + facing.ordinal(), 1);	//On joue l'animation d'attaque (de tir) correspondant à la direction
          throwing = true;		//On est en train de tirer
          this.target = target;	//On stocke la référence à la cible
          cooldown = maxCooldown;
        }
        else
        {
          target = null;
        }
      }
    }
  }


  void step() //Méthode exécutée à chaque frame
  {
    super.step();	//On appelle la méthode de la classe mère
    if (throwing && animator.getCurrentAnimation() != 8 + facing.ordinal()) //Si l'on est en train de tirer et que l'animation n'est plus celle du tir (elle a fini de jouer)
    {
      thrown = true;		//On passe en phase où le tir est lancé
      throwing = false;	//On n'est plus en train de tirer
      pj.throwAt(x, y, angle, this.target);	//On lance le projectile
    }

    if (thrown) //Si le projectile est tiré
    {
      pj.step();	//Il exécute son code
      if (pj.isDone() || distanceTo(pj) > range) //S'il est trop loin ou s'il a touché
      {
        thrown = false;		//On réinitialise l'état
        pj.reset();			//On réinitialise le projectile
      }
    }
  }

  void forward()	//Méthode pour avancer
  {
    if (!thrown && !throwing) //Si on est dans un état où l'on peut bouger
    {
      super.forward();	//On exécute la méthode de la classe mère
    }
  }

  void turn(float angle)	
  {
    if (!thrown && !throwing)	//Si on peut bouger
    {
      super.turn(angle);	//On exécute la méthode de la classe mère
    }
  }

  void disp()	//Méthode pour afficher
  {
    super.disp();	//On exécute la méthode de la classe mère
    if (thrown)	   //Si la projectile est lancé
    {
      pj.disp();	//On l'affiche
    }
  }

  float getDamage()
  {
    return pj.getDamage();
  }

  float getMinRange()
  {
    return minRange;
  }
}

class Projectile extends Locable //Classe gérant les projectiles
{
  protected float angle, speed;		//Angle est vitesse
  protected int damage;				//Dégats causés
  protected AnimationHandler animations ; //Animations  de vol et à l'impact
  protected Locable target;				 //Cible
  protected boolean hitting, done;			 //Booléens gérant l'état
  protected Direction direction;

  Projectile(float speed, int damage, AnimationHandler animations) //Constructeur
  {	
    super(0, 0);				//On appelle le constructeur de la classe mère
    this.angle = 0;			//On initialise les attributs
    this.speed = speed;
    this.damage = damage;
    this.target = target;
    this.animations = animations;
    this.hitting = false;
    this.done = false;
    this.direction = Direction.Up;
  }

  Projectile(Projectile source) //Constructeur de copie
  {
    super(source.x, source.y);
    this.angle = source.angle;			//On initialise les attributs
    this.speed = source.speed;
    this.damage = source.damage;
    this.animations = new AnimationHandler(source.animations);
    this.target = source.target;
    this.hitting = source.hitting;
    this.done = source.done;
  }

  void throwAt(float x, float y, float angle, Locable target) //Méthode pour lancer le projectile
  {
    this.target = target;		//On met à jours les attributs
    this.x = x;
    this.y = y;
    this.angle = angle;
    if ( abs(sin(angle)) >= abs(cos(angle)) )	//En fonction de la direction générale de l'angle on met à jour la direction du sprite
    {
      if (sin(angle) < 0)
      {
        direction = Direction.Down;
      }
      else
      {
        direction = Direction.Up;
      }
    }
    else
    {
      if (cos(angle) < 0)
      {
        direction = Direction.Left;
      }
      else
      {
        direction = Direction.Right;
      }
    }
    this.animations.setDefaultAnimation(direction.ordinal());
  }

  void step()		//Méthode pour effectuer une étape
  {
    if (!hitting) //Si l'on ne touche pas
    {
      x += cos(angle) * speed;	//On avance selon la vitesse et l'angle
      y += sin(angle) * speed;
    }

    if (!done && distanceTo(target) < 50  && !hitting)	//Si l'on n'a pas fini et que l'on touche l'ennemi
    {
      hitting = true;
      animations.playAnimation(4+direction.ordinal(), 1);
    }

    if (hitting)
    {
      if (animations.getCurrentAnimation() == animations.getDefaultAnimation())	//Si on a fini de jouer l'animation d'impact
      {
        if (target instanceof Destroyable)
          ((Destroyable)target).reduceHP(damage);	//On inglige les dégâts
        done = true;				//On a fini
      }
    }
  }

  void disp()	//FonctiMéthodeon d'affichage
  {
    animations.disp(x, y);	//On joue l'animation de vol
    animations.step();
  }

  boolean isDone()	//Assesseur
  {
    return done;
  }

  void reset()	//Méthode de réinitialisation
  {
    animations.reset();
    hitting = false;
    done = false;
  }

  float getDamage()
  {
    return damage;
  }
}