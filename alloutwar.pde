//Fichier principal où est lancée l'exécution du programme

void setup()
{
  //Affichage de la fenêtre en plein centre de l'écran avec le titre approprié

  size(600, 600);
  surface.setTitle("AllOutWar");
  surface.setResizable(true);
  surface.setLocation((displayWidth/2)-300, (displayHeight)/2-300);

  //Initialisation des ArrayList 

  units = new ArrayList<Unit>();
  buildings = new ArrayList<Building>();
  selectedUnits = new ArrayList<Unit>();

  availableBuildings = new ArrayList<Building>();

  images = new ArrayList<PImage>();
  loadedImages = new ArrayList<String>();

  menuButtons = new ArrayList<GImageButton>();        //Liste des boutons du menu
  inGameButtons = new ArrayList<GImageButton>();     //Liste des boutons en jeu
  buttonsBoolean = new ArrayList<Boolean>();        //Liste des booleans
  actionButtons = new ArrayList<GButton>();        //Liste des boutons d'action
  buildingButtons = new ArrayList<GButton>();
  trainingButtons = new ArrayList<GButton>();

  background(109, 7, 26);
  frameRate(80);

  /*Début GUI */


  //Variables boutons et texte
  hBtn = 70;
  wBtn = 300;
  dimActionBtn = 50;  
  textSize(16);

  //#### ICI les Boutons ####\\
  G4P.setGlobalColorScheme(GCScheme.RED_SCHEME); //Couleur utilisée pour les controls (temporaire)
  GButton.useRoundCorners(false);

  //Mise des boutons du menu dans une liste
  menuButtons.add(btnPlay = new GImageButton(this, (width/2)-(wBtn/2), (height/2)-2*hBtn-30, new String[] { "Buttons/play_neutral.png", "Buttons/play_overflown.png", "Buttons/play_clicked.png" } ));
  menuButtons.add(btnHelp = new GImageButton(this, (width/2)-(wBtn/2), (height/2)-hBtn-10, new String[] { "Buttons/help_neutral.png", "Buttons/help_overflown.png", "Buttons/help_clicked.png" } ));  
  menuButtons.add(btnCredits = new GImageButton(this, (width/2)-(wBtn/2), (height/2)+10, new String[] { "Buttons/credits_neutral.png", "Buttons/credits_overflown.png", "Buttons/credits_clicked.png" } ));
  menuButtons.add(btnQuit = new GImageButton(this, (width/2)-(wBtn/2), (height/2)+hBtn+30, new String[] { "Buttons/quit_neutral.png", "Buttons/quit_overflown.png", "Buttons/quit_clicked.png" } ));

  //Mise des boutons d'action dans une liste
  actionButtons.add(btnAttack = new GButton(this, 20, height-dimActionBtn-20, dimActionBtn, dimActionBtn, "Attack"));
  actionButtons.add(btnMove = new GButton(this, dimActionBtn+28, height-dimActionBtn-20, dimActionBtn, dimActionBtn, "Move"));
  actionButtons.add(btnAutonomous = new GButton(this, 2*dimActionBtn+36, height-dimActionBtn-20, dimActionBtn, dimActionBtn, "Autonomous"));
  actionButtons.add(btnStop = new GButton(this, 3*dimActionBtn+44, height-dimActionBtn-20, dimActionBtn, dimActionBtn, "Stop"));

  //Mise des booleans du menu dans une liste
  buttonsBoolean.add(Play);            //0  Écran de jeu
  buttonsBoolean.add(Help);           //1   Écran d'aide
  buttonsBoolean.add(Credits);       //2    Écran des crédits
  buttonsBoolean.add(Menu);         //3     Écran principal
  buttonsBoolean.add(Quit);        //4      Écran de confirmation YES/NO
  buttonsBoolean.add(inGameMenu); //5       Écran de menu en jeu
  buttonsBoolean.add(mainMenu);  //6        Écran de confirmation YES/NO --> actions différentes

  //Mise des boutons du menu in-game dans une liste
  inGameButtons.add(btnInGameMenu = new GImageButton(this, 10, 10, new String[] { "Buttons/menu_neutral.png", "Buttons/menu_overflown.png", "Buttons/menu_clicked.png" } ));
  inGameButtons.add(btnMainMenu = new GImageButton(this, (displayWidth/2)-(wBtn/2), (displayHeight/2)+10, new String[] { "Buttons/quittomainmenu_neutral.png", "Buttons/quittomainmenu_overflown.png", "Buttons/quittomainmenu_clicked.png" } ));
  inGameButtons.add(btnResume = new GImageButton(this, (displayWidth/2)-(wBtn/2), (displayHeight/2)-2*hBtn-30, new String[] { "Buttons/resume_neutral.png", "Buttons/resume_overflown.png", "Buttons/resume_clicked.png" } ));

  //Autres boutons
  btnYes = new GImageButton(this, (width/2)-wBtn/3-20, (height/2)-(hBtn/4), new String[] { "Buttons/yes_neutral.png", "Buttons/yes_overflown.png", "Buttons/yes_clicked.png" } );
  btnNo = new GImageButton(this, (width/2)+20, (height/2)-(hBtn/4), new String[] { "Buttons/no_neutral.png", "Buttons/no_overflown.png", "Buttons/no_clicked.png" } );
  btnBack = new GImageButton(this, 10, 10, new String[] { "Buttons/back_neutral.png", "Buttons/back_overflown.png", "Buttons/back_clicked.png" } );

  //INITIALISATION
  setInvisible(inGameButtons, null, true);
 
  /*FIN GUI */

  PositionComparator = new Comparator<Locable>() //Initialisation du comparateur de positions
  {
    @Override //Permet de modifier le comportement de cette méthode pour et seulement pour cet objet
    public int compare(Locable l1, Locable l2)  //Méthode comparant deux objets Locable selon leur position
    {
      return ( (Float)(l2.getY()) ).compareTo( (Float)(l1.getY()) );
    }
  };
  
  loadMap("Maps/map1.map");                              //Chargement de la carte
  loadAvailableBuildings("Factions/humanBuildings.avl");  //Chargement des bâtiments que le joueur peut placer
  generateBuildingButtons();                           //Génération des boutons pour placer ces bâtiments
}

void draw() 
{
  uiPosition();                  //Mise à jour du positionnement des éléments de l'interface

  //Affichage des boutons selon l'état du jeu

  displayConfirmationButtons();  
  displayActionButtons();
  displayBuildingButtons();
  displayTrainingButtons();

  //Conditions gérant l'affichage des différents écrans selon les booléens les gérant

  if (buttonsBoolean.get(3))
  { //MENU --> Reset le jeu
    background(109, 7, 26);
    frameRate(60);
    funFunction();
  }

  if (buttonsBoolean.get(4))
  { //QUIT
    background(109, 7, 26);
    canevas(350, 80);
    funFunction();
    fill(165, 7, 50);
    textAlign(CENTER);
    if (mainMenu) 
    {
      text("Are you sure you want to go back to the main menu ?", pTx, pTy-hBtn/4-35);
    }
    else 
    {
      text("Are you sure you want to quit AllOutWar ?", pTx, pTy-hBtn/4-35);
    }
  }

  if (buttonsBoolean.get(1))
  { //HELP
    background(109, 7, 26);
    funFunction();
    fill(165, 7, 50);
    textAlign(CENTER);
    text("See the guides in Guides folder. More help coming later ;)", pTx, pTy);
  }

  if (buttonsBoolean.get(2))
  { //CREDITS
    background(109, 7, 26);
    funFunction();
    fill(165, 7, 50);
    text("Le jeu AllOutWar vous est présenté par :", pTx, pTy-60);
    text("Virgil Marionneau", pTx, pTy-30);
    text("Armand Garnier-Le Breton", pTx, pTy-10);
    text("Arthur Stephant--Durand", pTx, pTy+10);
    text("Awen Jacq-Bodet (Graphismes)", pTx, pTy+30);
    text("Élèves de TS4 au lycée Clemenceau de Nantes (2020)", pTx, pTy+60);
  }

  if (buttonsBoolean.get(5))
  { //MENU IN-GAME
    background(109, 7, 26);
    canevas(350, 410);    
    funFunction();
  }

  if (buttonsBoolean.get(0))
  { //PLAY --> La boucle de jeu principale 

    background(109, 7, 26); //Affichage d'un fond uni
    frameRate(24);          //Mise à jour des fps

    pushMatrix();         //Sauvegarde des transformations neutres

    translate(travelX, travelY); //Déplacement compensant le zoom et les systèmes de coordonnées différents
                                //entre la fenêtre et le monde du jeu
    scale(scaleFactor);               //Zoom

    if(fog)
      showFogOfWar();

    if (placingBuilding)          //Si le joueur est en train de placer un bâtiment
      showBuildableZone();            //Affichage de la zone où le joueur peut construire

    handleBuildings();              //Gestion des bâtiments
    handleUnits();                  //Gestion des unités
    removeDeadSelection();          //Nettoyage de la sélection en cas de mort d'entités sélectionnées
    renderLimits();
    renderSelected();              //Affichage de la sélection sur les unités et bâtiments

    if (placingBuilding)          //Si le joueur est en train de placer un bâtiment
      renderPlacing();           //Affichage du bâtiment à placer sur la souris

    popMatrix();              //Annulation des transformations précedentes
    displayResource();        //Affichage des ressources du joueur 

    //Affichage de la barre de boutons de jeu et des informations sur la sélection

    stroke(50, 50, 50);
    fill(50, 50, 50);
    rect(0, height-dimActionBtn-30, width, dimActionBtn+30);
    showSelectedInfo();

    handleTravel();
  }
}

void handleUnits() //Procédure gérant les actions des unités, leur affichage et leur mort
{
  ArrayList<Integer> toKill = new  ArrayList<Integer>(); //Liste des unités mortes après ce tour
  for (int i = 0; i < units.size(); i++)  
  {
    units.get(i).step();
    if(units.get(i).getTeam().equals(playerTeam) || !fog)
    {
      units.get(i).disp();
    }
    else 
    { 
      Destroyable p = new Destroyable(units.get(i).getX(), units.get(i).getY(),1,1,1,playerTeam);
      if(p.distanceTo(p.nearestAllyEntity()) < sightRadius)
      {
        units.get(i).disp();
      }
    }
    if (units.get(i).getHP() == 0) //Si l'unité est morte
    {
      toKill.add(i);  //On l'ajoute à la liste des unités à supprimer
    }
  }
  Collections.sort(toKill);     //On trie la liste des unités mortes
  Collections.reverse(toKill);  //On l'inverse pour supprimer depuis la fin de la liste et ne pas changer la position 
  //d'unités encore à supprimer

  for (int i = 0; i < toKill.size(); i++)  //On retire les unités mortes
  {
    units.remove(toKill.get(i).intValue());
  }
}

void showFogOfWar()
{
  background(0);
  stroke(109, 7, 26);
  fill(109, 7, 26);
  for (int i = 0; i < buildings.size(); i++)  //On parcourt les bâtiments
  {
    if (buildings.get(i).getTeam().equals(playerTeam)) //Si le bâtiment appartient au joueur et n'est pas en construction
    { 
      //On affiche un disque centré sur le centre du bâtiment
      ellipse(buildings.get(i).getX() + buildings.get(i).getSizeX()/2, - buildings.get(i).getY() + buildings.get(i).getSizeY()/2, 2*sightRadius, 2*sightRadius);
    }
  }
  for (int i = 0; i < units.size(); i++)  //On parcourt les bâtiments
  {
    if (units.get(i).getTeam().equals(playerTeam)) //Si le bâtiment appartient au joueur et n'est pas en construction
    { 
      //On affiche un disque centré sur le centre du bâtiment
      ellipse(units.get(i).getX() + units.get(i).getSizeX()/2, - units.get(i).getY() + units.get(i).getSizeY()/2, 2*sightRadius, 2*sightRadius);
    }
  }
}

void handleBuildings() //Procédure gérant les actions des bâtiments, leur affichage et leur mort
{
  //Cette procédure fonctionne comme la précédente mais agit sur les bâtiments

  ArrayList<Integer> toDestroy = new  ArrayList<Integer>();

  for (int i = 0; i < buildings.size(); i++)
  {
    buildings.get(i).step();
    if(buildings.get(i).getTeam().equals(playerTeam) || !fog)
    {
      buildings.get(i).disp();
    }
    else 
    { 
      Destroyable p = new Destroyable(buildings.get(i).getX(), buildings.get(i).getY(),1,1,1,playerTeam);
      if(p.distanceTo(p.nearestAllyEntity()) < sightRadius)
      {
        buildings.get(i).disp();
      }
    }
    if (buildings.get(i) instanceof ResourceBuilding && buildings.get(i).getTeam().equals(playerTeam))
    {
     //Avec ces deux conditions on gère la production de ressources du joueur
      if ( ((ResourceBuilding)buildings.get(i)).getResource() == Resource.Iron ) 
      {
        playerIron += ((ResourceBuilding)buildings.get(i)).collect();
      }
      else if ( ((ResourceBuilding)buildings.get(i)).getResource() == Resource.Mana )
      {
        playerMana += ((ResourceBuilding)buildings.get(i)).collect();
      }
    } 
    if (buildings.get(i).getHP() == 0)
    {
      toDestroy.add(i);
    }
  }
  Collections.sort(toDestroy);
  Collections.reverse(toDestroy);
  for (int i = 0; i < toDestroy.size(); i++)
  {
    buildings.remove(toDestroy.get(i).intValue());
  }
}

void showBuildableZone()  //Procédure affichant la zone où le joueur peut construire
{
  for (int i = 0; i < buildings.size(); i++)  //On parcourt les bâtiments
  {
    if (buildings.get(i).getTeam().equals(playerTeam) && !(buildings.get(i) instanceof Construction)) //Si le bâtiment appartient au joueur et n'est pas en construction
    { 
      //On affiche un disque centré sur le centre du bâtiment
      stroke(54, 134, 26);
      fill(54, 134, 26);
      ellipse(buildings.get(i).getX() + buildings.get(i).getSizeX()/2, - buildings.get(i).getY() + buildings.get(i).getSizeY()/2, 2*buildRadius, 2*buildRadius);
    }
  }
}

void removeDeadSelection() //Procédure s'occupant de déselectionner les unités mortes
{
  //Fonctionne sur la même base quer les procédures handleUnits() et handleBuildings()

  ArrayList<Integer> toUnselect = new  ArrayList<Integer>();
  for (int i = 0; i < selectedUnits.size(); i++)
  {
    if (selectedUnits.get(i).getHP()==0)
    {
      toUnselect.add(i);
    }
  }
  Collections.sort(toUnselect);
  Collections.reverse(toUnselect);
  for (int i = 0; i < toUnselect.size(); i++)
  {
    selectedUnits.remove(toUnselect.get(i).intValue());
  }

  if (selectedBuilding != null && selectedBuilding.getHP() <= 0)
  {
    resetSelectedBuilding();
  }
}

void resetGame()  //Procédure réinitialisant le jeu
{
  playerIron = 500;
  playerMana = 500;
  units.clear();
  buildings.clear();
  selectedUnits.clear();
  selectedBuilding = null;
  loadMap("Maps/map1.map");
}

void addBuilding(Building b) //Procédure pour ajouter un bâtiment
{ 
  /*Permet de placer un bâtiment au bon endroit dans la liste selon sa position en abscisse
    une fonction similaire n'est pas nécessaire pour les unités car elles ne peuvent pas se superposer*/
  int size = buildings.size();
  if(size < 1)
  {
    buildings.add(b);
  }
  else 
  {
    int start = 0;
    int end = size - 1;
    int middle = min(size - 1,(int)(start + end + 1)/2);
    float y = b.getY();
    float m_y = buildings.get(middle).getY();

    while(start - end > 1) 
    {
      println(start," ",middle," ",end);
      if(y > m_y)
      {
       end = middle;
      }
      else if(y < m_y)
      {
        start = middle;
      }
      middle = (int)(start + end + 1)/2;
      m_y = buildings.get(middle).getY();
    }

    buildings.add(middle, b);
  }
}

void renderLimits() //Procédure affichant les limites de la carte
{
  noFill();
  stroke(0,255,255);
  rect(WEST,-SOUTH,EAST-WEST,SOUTH-NORTH);
}