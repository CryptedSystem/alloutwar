//Fichier contenant tous les imports et les variables et objets globaux

//Les imports de la bibliothèque LuaJ pour les IAs

import org.luaj.vm2.*;
import org.luaj.vm2.ast.*;
import org.luaj.vm2.compiler.*;
import org.luaj.vm2.lib.*;
import org.luaj.vm2.lib.jse.*;
import org.luaj.vm2.luajc.*;
import org.luaj.vm2.parser.*;
import org.luaj.vm2.script.*;
import org.luaj.vm2.server.*;

//Imports de la bibliothèque G4P pour les boutons

import g4p_controls.*;

//Imports de fonctions, procédures, classes et objets utiles de java (ArrayList, Collections, ...)

import java.util.*;

//Variables et objets globaux

GImageButton btnPlay, btnQuit, btnHelp, btnCredits, btnBack;   //Boutons du menu principal
GImageButton btnInGameMenu, btnResume, btnMainMenu;           //Bontons ingame
GImageButton btnYes, btnNo;                                  //Autres boutons
GButton btnAttack, btnAutonomous, btnMove, btnStop; //btnPatrol, btnKeepPositon... /* Boutons d'action des unités */

boolean Play=false, Help=false, Credits= false, Menu=true, Quit=false;   //Booleans des boutons du menu principal
boolean inGameMenu=false, mainMenu=false;    //Booleans des boutons ingame
boolean inGame=false;
boolean inMenu=true;

/* Liste des boutons d'action des unités */

ArrayList<GImageButton> menuButtons;
ArrayList<GImageButton> inGameButtons;
ArrayList<Boolean> buttonsBoolean;
ArrayList<GButton> buildingButtons;
ArrayList<GButton> trainingButtons;
ArrayList<GButton> actionButtons; 

float wBtn, hBtn;               //Hauteur|Largeur des boutons
float pTx, pTy;                //Position texte
float dimActionBtn;           //Taille boutons d'action des unités
float dimDynamicButton = 120;//Hauteur des boutons d'entrainement et de construction
float resourceX, resourceY; //Position des informations sur les ressources du joueur
int pWidth=0, pHeight=0;   //Taille précédente de la fenêtre


ArrayList<Unit> units;          //Unités sur la carte //
ArrayList<Building> buildings;  //Bâtiments sur la carte

ArrayList<PImage> images;       //Images chargées en mémoire
ArrayList<String> loadedImages; //Chemins des images chargées en mémoire

ArrayList<Unit> selectedUnits;     //Unités sélectionnées
Building selectedBuilding = null; // Bâtiment sélectionné

float sx1 = 0, sy1 = 0, sx2 = 0, sy2 = 0;       //Coordonnées de deux coins opposés du rectangle de sélection

//Booléens gérant l'état des actions en jeu       

boolean attackMode = false;             
boolean moveMode = false; 
boolean selecting = false;
boolean placingBuilding = false;
boolean training = false;

Building toPlace = null;          //Bâtiment à placer quand le joueur place un bâtiment

//Données sur le joueur

String playerTeam = "Team 1";   //Son équipe  
int playerIron = 500;           //Ses quantités de ressources
int playerMana = 500;
ArrayList<Building> availableBuildings; //Les bâtiments qu'il peut placer

float NORTH=1400, SOUTH=0, EAST=2000, WEST=0;  //Limites de la carte
float scaleFactor = 1.0/1.6;                    //Facteur de zoom
float travelX = 0, travelY = 0;						   //Facteurs de translation
float buildRadius = 350; //Rayon de la zone de construction autour d'un bâtiment du joueur
float sightRadius = 500;
boolean fog = true;

Comparator<Locable> PositionComparator; //Le comparateur de positions, son nom commence par une majuscule car c'est un objet unique